#! /bin/bash
set -eu

docheck() {
    local host="$1"; shift
    local -A users=()

    echo "# *** $host ***"
    ssh "$host" "sudo needrestart -b" |
	(while read -r key val1 rest; do
	     case "${key:0:-1}" in
		 NEEDRESTART-VER|NEEDRESTART-KSTA|NEEDRESTART-UCSTA)
		     ;;
		 NEEDRESTART-KCUR)
		     kcur="$val1"
		     ;;
		 NEEDRESTART-KEXP)
		     [[ $kcur != "$val1" ]] && echo "kernel $kcur != $val1"
		     ;;
		 NEEDRESTART-UCCUR)
		     uccur="$val1"
		     ;;
		 NEEDRESTART-UCEXP)
		     [[ $uccur != "$val1" ]] && echo "microcode $uccur != $val1"
		     ;;
		 NEEDRESTART-SVC)
		     echo "$val1"
		     ;;
		 NEEDRESTART-SESS)
		     users["$val1"]=1
		     ;;
		 *)
		     echo "unknown $key $val1 $rest"
		     ;;
	     esac
	     for u in "${!users[@]}"; do echo "$u"; done
	     done)
}

default() {
    for n in 01 02; do
	docheck logsrv-"${n}".sigsum.org
    done
    docheck jitsi-01.glasklarteknik.se
    for n in 01 02 03; do
	docheck kvm-"${n}".glasklarteknik.se
    done
    docheck mgmt-mlm-vg4-01.glasklarteknik.se
}

if [ $# -gt 0 ]; then
    for h in "$@"; do
	docheck "$h"
    done
else
    default
fi
