#! /bin/bash -eu
# https://salsa.debian.org/raspi-team/image-specs
# Tested on Debian 12.8 amd64

# Default Debian release. First argument overrides.
DEBIAN_RELEASE=bookworm; [ $# -gt 0 ] && { DEBIAN_RELEASE="$1"; shift; }
# Default image-specs git branch/tag/commit to use. Second argument overrides.
IMAGE_SPECS_PIN=ff7fdbf; [ $# -gt 0 ] && { IMAGE_SPECS_PIN="$1"; shift; }

sudo apt install -y vmdb2 dosfstools qemu-utils qemu-user-static debootstrap binfmt-support time kpartx bmap-tools python3 zerofree fakemachine
getent passwd builder || { sudo adduser builder; sudo usermod -a -G kvm builder; }
sudo -iu builder
[ -d image-specs ] || git clone --recursive https://salsa.debian.org/raspi-team/image-specs.git
cd image-specs
[ -z "$IMAGE_SPECS_PIN" ] || git switch "$IMAGE_SPECS_PIN"
make raspi_4_"$DEBIAN_RELEASE".img

# Example output from running in a VM with 4 vCPU's and 4GB RAM:
# All went fine.
# 1182.81user 205.07system 18:26.11elapsed 125%CPU (0avgtext+0avgdata 2208356maxresident)k
# 2776516inputs+6262472outputs (802major+128868minor)pagefaults 0swaps
# chmod 0644 raspi_4_bookworm.img
#
# builder@debian12:~/usr/src/image-specs$ ls -lh raspi_4_bookworm.img
# -rw-r--r-- 1 builder builder 2.5G Dec 10 17:33 raspi_4_bookworm.img
