nextcloud-01.sigsum.org (2022-08-23) -- VM @ logsrv-01

root@logsrv-01:~# ./mkvm.sh nextcloud-01.sigsum.org 2 4096 40
...
root@logsrv-01:~# lvcreate -n nextcloud -L 30G logsrv-01-nvme
  Logical volume "nextcloud" created.
root@logsrv-01:~# virsh attach-disk nextcloud-01.sigsum.org /dev/logsrv-01-nvme/nextcloud vdb --live --config --subdriver raw --sourcetype block
Disk attached successfully

pvcreate /dev/vdb
vgcreate nextcloud-vg /dev/vdb
lvcreate -n ncdata -l 100%FREE nextcloud-vg
mkfs.ext4 /dev/nextcloud-vg/ncdata
echo "UUID=$(lsblk -no UUID /dev/nextcloud-vg/ncdata) /var/nextcloud ext4 defaults 0 2" >> /etc/fstab
mkdir /var/nextcloud
mount -a

apt install php-fpm php-zip php-pgsql php-bz2 php-intl php-imagick php-bcmath php-gmp

cd /tmp
curl -sL https://nextcloud.com/nextcloud.asc | gpg --import
gpg --list-key 28806A878AE423A28372792ED75899B9A724937A
curl -LO https://download.nextcloud.com/server/releases/nextcloud-24.0.4.tar.bz2
curl -LO https://download.nextcloud.com/server/releases/nextcloud-24.0.4.tar.bz2.asc
gpg --verify nextcloud-24.0.4.tar.bz2.asc nextcloud-24.0.4.tar.bz2
tar x -C /var/nextcloud -f nextcloud-24.0.4.tar.bz2
mv /var/nextcloud/nextcloud /var/nextcloud/www
chown -R www-data:www-data /var/nextcloud/www

echo "deb https://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list
curl -s https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add -
apt update
apt install postgresql
sudo -u postgres psql
> CREATE USER nc_admin WITH PASSWORD 'some-secret';
> CREATE DATABASE nextcloud TEMPLATE template0 ENCODING 'UNICODE';
> ALTER DATABASE nextcloud OWNER TO nc_admin;
> GRANT ALL PRIVILEGES ON DATABASE nextcloud TO nc_admin;
> \q

# https://docs.nextcloud.com/server/latest/admin_manual/installation/command_line_installation.html
# https://docs.nextcloud.com/server/latest/admin_manual/configuration_server/occ_command.html#command-line-installation-label
export ADMIN_USER=admin$NN
cd /var/nextcloud/www
sudo -u www-data php occ maintenance:install --database pgsql --database-name nextcloud --database-user nc_admin --admin-user $ADMIN_USER

: root@nextcloud-01:/var/nextcloud/www# sudo -u www-data php occ maintenance:install --database pgsql --database-name nextcloud --database-user nc_admin --admin-user $ADMIN_USER
: What is the password to access the database with user <nc_admin>?
: What is the password you like to use for the admin account <$ADMIN_USER>?
: Nextcloud was successfully installed

sudo -u www-data php occ config:system:set trusted_domains 0 --value=nc.sigsum.org
sudo -u www-data php occ config:system:set trusted_domains 1 --value=nextcloud.sigsum.org

: root@nextcloud-01:/var/nextcloud/www# sudo -u www-data php occ config:system:get trusted_domains
: nc.sigsum.org
: nextcloud.sigsum.org

while read -r var val; do sudo -u www-data php occ config:system:set "$var" "$val"; done <<EOF
mail_smtpmode --value=sendmail
mail_sendmailmode --value=smtp
mail_from_address --value=nextcloud-admin
mail_domain --value=sigsum.org
default_phone_region --value=SE
overwrite.cli.url --value=https://nc.sigsum.org/
default_locale --value=sv_SE
token_auth_enforced --value=true
logtimezone --value=Europe/Stockholm
EOF

Set the following by editing config/config.php -- the quoting became
just a bit too crazy for me right now.

: 'memcache.local' => '\\OC\\Memcache\\APCu',
: 'remember_login_cookie_lifetime' => 60 * 60 * 24 * 1
: 'session_lifetime' => 60 * 60 * 12
: 'trashbin_retention' => 'auto, 7'

echo '*/5    *       *       *       *       /usr/bin/php -f /var/nextcloud/www/cron.php' | crontab -u www-data -

# https://docs.nextcloud.com/server/latest/admin_manual/installation/harden_server.html#setup-fail2ban
apt install fail2ban
cat > /etc/fail2ban/filter.d/nextcloud.conf <<EOF
[Definition]
_groupsre = (?:(?:,?\s*"\w+":(?:"[^"]+"|\w+))*)
failregex = ^\{%(_groupsre)s,?\s*"remoteAddr":"<HOST>"%(_groupsre)s,?\s*"message":"Login failed:
            ^\{%(_groupsre)s,?\s*"remoteAddr":"<HOST>"%(_groupsre)s,?\s*"message":"Trusted domain error.
datepattern = ,?\s*"time"\s*:\s*"%%Y-%%m-%%d[T ]%%H:%%M:%%S(%%z)?"
EOF
cat > /etc/fail2ban/jail.d/nextcloud.local <<EOF
[nextcloud]
backend = auto
enabled = true
port = 80,443
protocol = tcp
filter = nextcloud
maxretry = 5
bantime = 3600
findtime = 3600
logpath = /var/nextcloud/www/data/nextcloud.log
banaction = nftables
EOF
systemctl reload fail2ban
fail2ban-client status nextcloud

# For SVG support in imagick, do
apt install libmagickcore-6.q16-6-extra
