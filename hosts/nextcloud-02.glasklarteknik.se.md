# nextcloud-02.glasklarteknik.se.md 2024-08-06

Upgrading the Nextcloud software will probably take some time. Since
people might need the service (calendar, contacts and potentially
files) we will reinstall rather than upgrade.

## plan

nextcloud-01.so is running Nextcloud 25.0.13.2.
```
root@nextcloud-01:~# (cd /var/nextcloud/www && sudo -u www-data php occ status)
  - installed: true
  - version: 25.0.13.2
  - versionstring: 25.0.13
  - edition:
  - maintenance: false
  - needsDbUpgrade: false
  - productname: Nextcloud
  - extendedSupport: false
```

Latest release is 29. We will have to upgrade in four steps:
- https://docs.nextcloud.com/server/latest/admin_manual/release_notes/upgrade_to_26.html
  - https://download.nextcloud.com/server/releases/latest-26.tar.bz2
- https://docs.nextcloud.com/server/latest/admin_manual/release_notes/upgrade_to_27.html
  - https://download.nextcloud.com/server/releases/latest-27.tar.bz2
- https://docs.nextcloud.com/server/latest/admin_manual/release_notes/upgrade_to_28.html
  - https://download.nextcloud.com/server/releases/latest-28.tar.bz2
- no notes available for 29
  - https://download.nextcloud.com/server/releases/latest-29.tar.bz2

```
26.0.13 https://download.nextcloud.com/server/releases/nextcloud-26.0.13.tar.bz2
27.0.11 https://download.nextcloud.com/server/releases/nextcloud-27.1.11.tar.bz2
28.0.7 https://download.nextcloud.com/server/releases/nextcloud-28.0.7.tar.bz2
29.0.4 https://download.nextcloud.com/server/releases/nextcloud-29.0.4.tar.bz2
```

https://docs.nextcloud.com/server/latest/admin_manual/maintenance/upgrade.html

We're running postgresql-{14,15,16} from postgresql.org, presumably
because bullseye didn't have modern enough pgsql at the time of
installation (2022-08-23). But why all of them?
```
root@nextcloud-01:~# cat /etc/apt/sources.list.d/pgdg.list
deb https://apt.postgresql.org/pub/repos/apt bullseye-pgdg main
```

Bookworm has postgresql-15. Which version is the nextcloud db?
The size of the data directories indicate 14:
```
root@nextcloud-01:~# du -sh /var/lib/postgresql/*
135M    /var/lib/postgresql/14
39M     /var/lib/postgresql/15
39M     /var/lib/postgresql/16
```

What pgsql versions do Nextcloud support?

[System requirements](https://docs.nextcloud.com/server/latest/admin_manual/installation/system_requirements.html)
says "PostgreSQL 12/13/14/15/16" so moving to Debian's postgresql-15 sounds like a good choice.

Database migration from a pgsql perspective:
[19.6. Upgrading a PostgreSQL Cluster](https://www.postgresql.org/docs/current/upgrading.html)
says `pg_dumpall` but also links to [pg_upgrade](https://www.postgresql.org/docs/current/pgupgrade.html).

## execution

- [x] lower TTL for nextcloud.{gt.se,s.o}
- [x] add A and AAAA for nextcloud-02.glasklarteknik.se
- [x] create VM nextcloud-02.glasklarteknik.se on kvm-01.gtse
  mkvm.sh nextcloud-02.glasklarteknik.se 2 4096 40
  NOTE: locale en_US.UTF-8 is important for postgres default database locale
- [x] limit access to ports 80,443
- [x] install postgresql-15
```
localectl | rg LANG=en_US.UTF-8 || echo BZZZZT
apt install postgresql-15
mkdir -p /var/backups/postgresql
chmod 700 /var/backups/postgresql
cat > /usr/sbin/pgdump.sh <<'EOF'
#! /bin/sh
set -eu
dumpfile=/var/backups/postgresql/pgdumpall.sql.xz
dumpdir="$(dirname $dumpfile)"
[ -d "$dumpdir" ] || mkdir -p "$dumpdir"
chmod 700 "$dumpdir"
cd /tmp # make sure user postgres can cd to $PWD
sudo -u postgres pg_dumpall --clean | xz > "$dumpfile"
chmod 600 "$dumpfile"
EOF
chmod +x /usr/sbin/pgdump.sh
```
- [x] export db on -01 and import it on -02
  - export using pgdump.sh (with pg_dumpall --clean)
  - import with 'psql -f - postgres', as user postgres
    xz -cd .priv/pgdump.sql.xz | sudo -u postgres psql -f - postgres
- [x] prepare for installing nextcloud from a tar ball
```
mkdir -p /var/nextcloud/nextcloud
chown www-data:www-data /var/nextcloud/nextcloud
curl -sL https://nextcloud.com/nextcloud.asc | gpg --import
gpg --list-key 28806A878AE423A28372792ED75899B9A724937A || echo BZZZZT
```
- [x] install nextcloud-26 dependencies
  apt install php-fpm php-zip php-pgsql php-bz2 php-intl php-imagick php-bcmath php-gmp
- [x] install nextcloud-26, the files
```
export NCMAJOR=26
curl -LO https://download.nextcloud.com/server/releases/latest-${NCMAJOR}.tar.bz2
curl -LO https://download.nextcloud.com/server/releases/latest-${NCMAJOR}.tar.bz2.asc
gpg --verify latest-${NCMAJOR}.tar.bz2.asc latest-${NCMAJOR}.tar.bz2 && (
tar x -C /var/nextcloud -f latest-${NCMAJOR}.tar.bz2
chown -R www-data:www-data /var/nextcloud/nextcloud)
```
- [x] install nextcloud-26
```
: use NN to *not* be the same as in the previous instance
export ADMIN_USER=admin$NN
(cd /var/nextcloud/nextcloud && sudo -u www-data php occ maintenance:install --database pgsql --database-name nextcloud --database-user nc_admin --admin-user $ADMIN_USER)
(cd /var/nextcloud/nextcloud && sudo -u www-data php occ upgrade)
```
- [x] configure nextcloud
```
(
cd /var/nextcloud/nextcloud;
sudo -u www-data php occ config:system:set trusted_domains 0 --value=nextcloud.glasklarteknik.se
while read -r var val; do sudo -u www-data php occ config:system:set "$var" --value="$val"; done <<'EOF'
mail_smtpmode sendmail
mail_sendmailmode smtp
mail_from_address nextcloud-admin
token_auth_enforced true
mail_domain glasklarteknik.se
default_phone_region SE
default_locale sv_SE
logtimezone Europe/Stockholm
EOF
)
: temporarily allow nextcloud-02.gtse
(cd /var/nextcloud/nextcloud && sudo -u www-data php occ config:system:set trusted_domains 1 --value=nextcloud-02.glasklarteknik.se)

:set the following by editing config/config.php
'memcache.local' => '\\OC\\Memcache\\APCu',
'remember_login_cookie_lifetime' => 60 * 60 * 24 * 1,
'session_lifetime' => 60 * 60 * 12,
'trashbin_retention' => 'auto, 7',

echo '*/5    *       *       *       *       /usr/bin/php -f /var/nextcloud/nextcloud/cron.php' | crontab -u www-data -

systemctl restart php8.2-fpm
```

- [x] copy user data
  for d in anne-lee@amagicom.se  fredrik@glasklarteknik.se    kai.michaelis@immu.ne    marie@amagicom.se        philipp.deppenwiese@immu.ne fredrik@amagicom.se   jens.drenhaus@9elements.com  linus@glasklarteknik.se  nisse@glasklarteknik.se  rgdd@glasklarteknik.se; do 
    rsync -e "ssh -p 4722 -i /root/.ssh/temp-wwwdata -l tmpcopy" -t -avz --delete nextcloud-01.sigsum.org:/var/nextcloud/www/data/$d/ /var/nextcloud/nextcloud/data/$d; done
- [x] configure nextcloud part II
Logged in as adminNN at https://nextcloud-02.glasklarteknik.se/:

- set email address for adminNN to nextcloud-admin@glasklarteknik.se
- enable 2FA for adminNN

- [x] upgrade to nextcloud-27
```
export NCMAJOR=27
curl -LO https://download.nextcloud.com/server/releases/latest-${NCMAJOR}.tar.bz2
curl -LO https://download.nextcloud.com/server/releases/latest-${NCMAJOR}.tar.bz2.asc
gpg --verify latest-${NCMAJOR}.tar.bz2.asc latest-${NCMAJOR}.tar.bz2 || echo BZZZZT
tar x -C /var/nextcloud -f latest-${NCMAJOR}.tar.bz2
chown -R www-data:www-data /var/nextcloud/nextcloud
(cd /var/nextcloud/nextcloud && sudo -u www-data php occ upgrade)
```
- [x] upgrade to nextcloud-28
```
export NCMAJOR=28
curl -LO https://download.nextcloud.com/server/releases/latest-${NCMAJOR}.tar.bz2
curl -LO https://download.nextcloud.com/server/releases/latest-${NCMAJOR}.tar.bz2.asc
gpg --verify latest-${NCMAJOR}.tar.bz2.asc latest-${NCMAJOR}.tar.bz2 && (
tar x -C /var/nextcloud -f latest-${NCMAJOR}.tar.bz2
chown -R www-data:www-data /var/nextcloud/nextcloud
cd /var/nextcloud/nextcloud && sudo -u www-data php occ upgrade
)
```
- [x] upgrade to nextcloud-29
```
export NCMAJOR=29
curl -LO https://download.nextcloud.com/server/releases/latest-${NCMAJOR}.tar.bz2 && \
curl -LO https://download.nextcloud.com/server/releases/latest-${NCMAJOR}.tar.bz2.asc && \
gpg --verify latest-${NCMAJOR}.tar.bz2.asc latest-${NCMAJOR}.tar.bz2 && (
tar x -C /var/nextcloud -f latest-${NCMAJOR}.tar.bz2 && \
chown -R www-data:www-data /var/nextcloud/nextcloud && \
cd /var/nextcloud/nextcloud && \
  sudo -u www-data php occ upgrade && \
  sudo -u www-data php -f /var/nextcloud/nextcloud/cron.php
)
```
- [x] fail2ban
  - https://docs.nextcloud.com/server/latest/admin_manual/installation/harden_server.html#setup-fail2ban
```
apt install -y fail2ban
cat > /etc/fail2ban/filter.d/nextcloud.conf <<'EOF'
[Definition]
_groupsre = (?:(?:,?\s*"\w+":(?:"[^"]+"|\w+))*)
failregex = ^\{%(_groupsre)s,?\s*"remoteAddr":"<HOST>"%(_groupsre)s,?\s*"message":"Login failed:
            ^\{%(_groupsre)s,?\s*"remoteAddr":"<HOST>"%(_groupsre)s,?\s*"message":"Trusted domain error.
datepattern = ,?\s*"time"\s*:\s*"%%Y-%%m-%%d[T ]%%H:%%M:%%S(%%z)?"
EOF
cat > /etc/fail2ban/jail.d/nextcloud.local <<'EOF'
[nextcloud]
backend = auto
enabled = true
port = 80,443
protocol = tcp
filter = nextcloud
maxretry = 5
bantime = 3600
findtime = 3600
logpath = /var/nextcloud/data/nextcloud.log
banaction = nftables
EOF
mv /etc/fail2ban/jail.d/defaults-debian.conf /etc/fail2ban/jail.d/defaults-debian.conf.OFF
systemctl enable --now fail2ban
fail2ban-client status nextcloud
```
- [x] maybe inform users of the upcoming change
- [x] freeze -01, ie no external (write) access
- [ ] sync db and user data again
```
nextcloud-01
sudo -u postgres pg_dump --data-only nextcloud | xz > ncpgsync.sql.xz

nextcloud-02
sudo -u www-data bash -c "cd /var/nextcloud/nextcloud/ && php occ maintenance:mode --on"
xz -cd ncpgsync.sql.xz | sudo -u postgres psql -f - nextcloud
```

This didn't work well, since the database doesn't look the same now as
it did, and does on -01.

Starting over from scratch.

- [x] clean up and prepare for installation
```
systemctl stop apache2 php8.2-fpm
rm -rf /var/nextcloud/nextcloud /var/nextcloud/data
curl -sL https://nextcloud.com/nextcloud.asc | gpg --import
gpg --list-key 28806A878AE423A28372792ED75899B9A724937A || echo BZZZZT
```
- [x] sync db
```
-01
sudo -u postgres pg_dumpall --clean | xz > pgdump.sql.xz

-02
xz -cd pgdump.sql.xz | sudo -u postgres psql -f - postgres
```
- [x] install 26
```
export NCMAJOR=26
curl -LO https://download.nextcloud.com/server/releases/latest-${NCMAJOR}.tar.bz2
curl -LO https://download.nextcloud.com/server/releases/latest-${NCMAJOR}.tar.bz2.asc
gpg --verify latest-${NCMAJOR}.tar.bz2.asc latest-${NCMAJOR}.tar.bz2 && (
tar x -C /var/nextcloud -f latest-${NCMAJOR}.tar.bz2
chown -R www-data:www-data /var/nextcloud/nextcloud
cd /var/nextcloud/nextcloud
sudo -u www-data php occ maintenance:install --database pgsql --database-name nextcloud --database-user nc_admin --admin-user $ADMIN_USER
sudo -u www-data php occ config:system:set datadirectory --value=/var/nextcloud/data
mv data ../
sudo -u www-data php occ upgrade
)
```
- [x] copy user data
```
for d in anne-lee@amagicom.se fredrik@glasklarteknik.se kai.michaelis@immu.ne marie@amagicom.se philipp.deppenwiese@immu.ne fredrik@amagicom.se jens.drenhaus@9elements.com linus@glasklarteknik.se nisse@glasklarteknik.se rgdd@glasklarteknik.se; do
    rsync -e "ssh -p 4722 -i /root/.ssh/temp-wwwdata -l tmpcopy" -t -az nextcloud-01.sigsum.org:/var/nextcloud/www/data/$d/ /var/nextcloud/data/$d
done
```
- [x] configure nextcloud
```
cd /var/nextcloud/nextcloud
sudo -u www-data php occ config:system:set trusted_domains 0 --value=nextcloud.glasklarteknik.se
while read -r var val; do sudo -u www-data php occ config:system:set "$var" --value="$val"; done <<'EOF'
mail_smtpmode sendmail
mail_sendmailmode smtp
mail_from_address nextcloud-admin
token_auth_enforced true
mail_domain glasklarteknik.se
default_phone_region SE
default_locale sv_SE
logtimezone Europe/Stockholm
EOF

: temporarily allow nextcloud-02.gtse
sudo -u www-data php occ config:system:set trusted_domains 1 --value=nextcloud-02.glasklarteknik.se

: set the following by editing config/config.php:
'memcache.local' => '\\OC\\Memcache\\APCu',
'remember_login_cookie_lifetime' => 60 * 60 * 24 * 1,
'session_lifetime' => 60 * 60 * 12,
'trashbin_retention' => 'auto, 7',

echo '*/5    *       *       *       *       /usr/bin/php -f /var/nextcloud/nextcloud/cron.php' | crontab -u www-data -

sudo -u www-data php occ db:add-missing-indices
apt install libmagickcore-6.q16-6-extra
systemctl start apache2 php8.2-fpm
```

Logged in as adminNN, at https://nextcloud-02.glasklarteknik.se/:

- disable old admin https://nextcloud-02.glasklarteknik.se/index.php/settings/users
- set email address for adminNN to nextcloud-admin@glasklarteknik.se https://nextcloud-02.glasklarteknik.se/index.php/settings/user
- enable 2FA for adminNN https://nextcloud-02.glasklarteknik.se/index.php/settings/user/security

- [x] upgrade to 27
```
tar x -C /var/nextcloud -f /tmp/latest-27.tar.bz2 && \
chown -R www-data:www-data /var/nextcloud/nextcloud && \
sudo -u www-data php occ upgrade
```

- [x] upgrade to 28
```
tar x -C /var/nextcloud -f /tmp/latest-28.tar.bz2 && \
chown -R www-data:www-data /var/nextcloud/nextcloud && \
sudo -u www-data php occ upgrade
```

- [x] upgrade to 29
```
tar x -C /var/nextcloud -f /tmp/latest-29.tar.bz2 && \
chown -R www-data:www-data /var/nextcloud/nextcloud && \
sudo -u www-data php occ upgrade && \
sudo -u www-data php occ db:add-missing-indices
```

- [x] fix various things
```
echo www-data: root >> /etc/aliases; newaliases
sudo -u www-data php occ maintenance:repair --include-expensive
sudo -u www-data php occ config:system:set maintenance_window_start --value=1
```
  - enable memcache https://docs.nextcloud.com/server/29/admin_manual/configuration_files/files_locking_transactional.html
  - remove all files listed as EXTRA_FILE in https://nextcloud-02.glasklarteknik.se/index.php/settings/integrity/failed

- [x] check background jobs
First run the periodic job:
```
sudo -u www-data "cd /var/nextcloud/nextcloud && php -f cron.php"
```

Then check the log at https://nextcloud-02.glasklarteknik.se/index.php/settings/admin/logging
- [x] install missing apps
  - https://nextcloud-02.glasklarteknik.se/index.php/settings/apps/security/twofactor_webauthn
- [x] test
  - [x] https://nextcloud.glasklarteknik.se/settings/admin/overview looks ok
  - [x] user data exists, including calendars and address books
  - [x] email is being sent to newly created users
- [x] update CNAME nextcloud.gt.se
  Done Fri Aug  9 13:36:20 CEST 2024, TTL was 300
- [x] remove CNAME nextcloud.s.o
- [x] open 80,443 to the world
- [x] retrieve certificate
```
rm /etc/letsencrypt/live/nextcloud.glasklarteknik.se
certbot certonly -d nextcloud-02.glasklarteknik.se,nextcloud.glasklarteknik.se --pre-hook "systemctl stop apache2" --post-hook "systemctl start apache2" --standalone --expand
```
- [x] remove cert from -01
```
ssh nextcloud-01.sigsum.org sudo shred -zun 13 /etc/letsencrypt/live/nextcloud.glasklarteknik.se/privkey.pem
ssh nextcloud-01.sigsum.org sudo mv /etc/letsencrypt/live/nextcloud.glasklarteknik.se /etc/letsencrypt/live/nextcloud.glasklarteknik.se.OFF
```
- [x] inform users
- [x] raise TTL for nextcloud.gt.se
- [x] clean up -01, ie stop apache and more
- [x] pretty urls
```
sudo -u www-data bash -c "cd /var/nextcloud/nextcloud && php occ config:system:set htaccess.RewriteBase --value=/"
sudo -u www-data bash -c "cd /var/nextcloud/nextcloud && php occ maintenance:update:htaccess"
```

## after the fact
I think we screwed up everyone's authentication. :(
This is probably to be expected on a new instance with a new
'instanceid', 'secret' and 'passwordsalt'.

In retrospect, it'd been better to just take the downtime and upgrade
Nextcloud on the existing VM to the point where Debian 11 wouldn't
work anymore (PHP dependencies, I suppose), upgrade Debian 11 -> 12,
and then continue upgrading Nextcloud to latest.

An alternative to that would've been to install a new VM with Debian
11, copy everything from the existing instance and go from there.
