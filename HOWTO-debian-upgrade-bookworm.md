How to upgrade a Glasklar system from Debian 11 (Bullseye) to 12 (Bookworm).

Inspiration has been taken from https://www.debian.org/releases/bookworm/amd64/release-notes/ch-upgrading.en.html

We're taking the opportunity to remove package areas 'contrib' and
'non-free' since we probably don't use them anywhere and also should
not use them without considering alternatives.

## Instructions

NOTE: Instructions are not complete for upgrading any of the non pets
systems listed in https://git.glasklar.is/glasklar/services/misc/-/issues/8 .

These instructions have been written in the form of a script but not
been made into a proper script because a human being needs to evaluate
the outcome of several commands before proceeding. But the format is
conventient for cut and paste.

```
cat /etc/debian_version | rg ^11\. || echo "ERROR: no can do"
touch /etc/ansible-wrapper-disabled
BUP_RATE_LIMIT=- my_bup.sh
dpkg-query -W -f='${Section}\t${Package}\n' | rg ^contrib\|^non-free && echo "WARNING: this host needs more than the main area"
apt update && apt upgrade
apt autopurge
apt list \~c; apt purge \~c
for f in sources.list sources.list.d/bullseye-backports.list sources.list.d/deb_torproject_org_torproject_org.list; do
	[ -f /etc/apt/"$f" ] || continue
    sed -i.bak -e 's/bullseye/bookworm/g' -e 's/ contrib//g' -e 's/ non-free//g' /etc/apt/"$f"
done
[ -f /etc/apt/sources.list.d/bullseye-backports.list ] && mv /etc/apt/sources.list.d/bullseye-backports.list /etc/apt/sources.list.d/bookworm-backports.list
apt update
apt -o APT::Get::Trivial-Only=true full-upgrade
apt upgrade --without-new-pkgs
apt full-upgrade
dpkg -l 'linux-image*' | grep ^ii | grep -i meta || echo "WARNING: no kernel package installed"
apt autopurge
apt list \~c; apt purge \~c
rm /etc/ansible-wrapper-disabled
history -a; reboot
```

After reboot, make sure the system is booted and accessible over
ssh. Check `systemctl --failed` and `journalctl -b -p warning` too.


## Notes

- New versions of a package may introduce new configuration. Keeping
  the local version currently installed is the safe bet. Taking a look
  at the diff first is a good idea for packages configured by Ansible
  since we might want to update Ansible roles to incorporate some of
  the changes.

- On identifying packages installed from the 'contrib' or 'non-free' archive areas
  - https://unix.stackexchange.com/questions/111086/list-all-software-installed-from-particular-component-non-free-contrib
  - https://www.debian.org/doc/debian-policy/ch-archive.html#s-subsections

- Removing 'contrib' and 'non-free' was triggered by apt update
  becoming so noisy when seeing 'non-free' but not
  'non-free-firmware'. This might have to do with the decision [1] to
  include non-free firmware in installers (and live images).

- See also bookworm-upgrade-notes.md for notes on the upgrade
  procedure for individual hosts.


[1] https://www.debian.org/vote/2022/vote_003

