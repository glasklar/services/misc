# How to install Debian on a physical machine

Goal: Install Debian (Bookworm) on a raid1 array ("mirror"), including
the EFI System Partition (aka ESP).

Assumption: The system is equiped with two identical SSD's for holding
system files, typically NVMe SSD's.

The essential problems are two:
1) grub-install does not like the idea of installing GRUB on an [mdadm][] array
2) EFI won't find the ESP on an mdadm device with the default RAID metadata layout

We solve 1 by installing GRUB to the first underlying device, while
not being part of the mirror.

We solve 2 by using an alternative metadata layout with the superblock
at the end of the device.

It remains to be seen what happens when the grub-efi-amd64* packages
are updated. grub-install will likely fail.

[mdadm]: https://en.wikipedia.org/wiki/Mdadm

## Installing Debian

Download debian-VERSION-amd64-netinst.iso.
Verify checksum file.
Verify checksum.
Boot the ISO, from a USB stick or from virtual media (BMC).

FIXME: select locale, timezone etc
Chose US keyboard layout.

Partition the two system disks as follows, using 'a' in the name for
the first disk and 'b' for the second. Set type to RAID on all
partitions.

```
 # size name
 1 512M esp-md-{a,b}
 2 2G   boot-md-{a,b}
 3 16G  swap-md-{a,b}
 4 *    pv-system-md-{a,b}
```

Create four md raid1 arrays, one per partition, and install the system
to them.

Create a VG named `vg_$hostname` and LV's named after the file systems
they will hold (root, var, srv).

When the GRUB installation fails, ignore that and finish the
installation without GRUB being installed.

Start a shell, chroot into /target (/dev/vg_$hostname/root) and mount
efivarfs (mount -t efivarfs efivarfs /sys/firmware/efi/efivars). If
this seems cumbersome, reboot into the installer ISO again and start a
rescue shell, which sets things up properly.

Break the mirror, install grub on one of the underlying partitions and
recreate the mirror with superblock version 1.0, storing the RAID
metadata at the end of the device. Adjust the mdadm and ssd device
names according to your setup.

```
umount /boot/efi
mdadm --stop md0
mount -t vfat /dev/nvme0n1p1 /boot/efi
grub-install
umount /boot/efi
mdadm --create /dev/md0 --level 1 -n 2 --metadata 1.0 /dev/nvme[01]n1p1
cat /proc/mdstat						# debug hint
/usr/share/mdadm/mkconf force-generate 	# updates /etc/mdadm/mdadm.conf
mount -a 								# checking that fstab is still good
update-initramfs -u
grub-mkconfig > /boot/grub/grub.cfg
```

Reboot into your new Debian system.

## Bootstrapping Ansible

Before bootstrapping Ansible, two things need to be done.

1) Add your own ssh key in /root/.ssh/authorized_keys and reload sshd.

2) Install a validating DNS resolver and make sure it works.

```
apt install unbound ldnsutils
printf "nameserver 127.0.0.1\noptions trust-ad edns0\n" > /etc/resolv.conf
drill -DT getuid.sigsum.org
...
[T] getuid.sigsum.org.	7200	IN	A	91.223.231.168
```

Bootstrap Ansible by running ansible/addhost on laptop/workstation.

## Manual steps required on the new system

1) Work around a few shortcomings in the Ansible setup

```
sudo rm /etc/nftables.conf
sudo ansible-my-wrapper
sudo systemctl reload nftables
sudo systemctl reload ssh
```

2) Add SSHFP DNS records

```
ssh-keygen -r $(hostname) | egrep "SSHFP [134] 2" | while read -r name in type n1 n2 fpr; do echo $name 7200 SSHFP $n1 $n2 $fpr; done
```

3) Initialise `borg`

See `which my_bup.sh` for the the single command that needs to be run,
as root. Save the generated key in the passdb.
