# Creating and managing VM's

## Creating a VM

  1. Ask Linus for hostname, IP address, netmask and default gateway.
     - (If IP address is A.B.C.D, you can set the installer's DNS server to
       A.B.C.1.)
  2. SSH to a KVM host (group `kvmhosts` in Ansible).
  3. `sudo mkvm.sh FQDN`
     - **Note:** the defaults are good only for smallish systems.
     - **Note:** if you're having trouble getting a network connection, the
       BRIDGE option might be wrong.
  4. Go through the Debian installer as usual.
     - Click cancel on the installer's network prompts (like DHCP) and then
       enter IP address and such manually based on the information from step 1.
     - Use a root password from passdb (`pass generate -n hosts/FQDN/root 32 &&
       pass git push`).  Don't install any other user than root.  (Click "back"
       and move forward to the next step manually after setting root password.)
     - Choose LVM if the system will have any storage requirements at all.
       Unless there is a good reason to have separate partitions for `/home`,
       `/var`, etc., just select the default "everything in one place" option.
     - Install ssh-server and standard system packages, disable all other
       possible install packages (especially ensure no "desktop" and "gnome").
  5. After reboot in the console:
      - Add your own ssh key in `/root/.ssh/authorized_keys` and make sure that
        /etc/ssh/sshd_config has PermitRootLogin prohibit-password.
        - **Hint:** `systemctl restart sshd`
      - `rm /etc/nftables.conf` (so that step 6 puts a sane default in place)
      - Send Linus the output of: `ssh-keygen -r $(hostname) | egrep "SSHFP
        [134] 2" | while read -r name in type n1 n2 fpr; do echo $name 7200
        SSHFP $n1 $n2 $fpr; done`
  6. See the "Preparations" step in the Ansible repository, which specifies what
     needs to be done to configure ansible and then actually install the VM.
     Before running ansible, install a validating DNS resolver as described in
     our [how-to][].
  7. Setup a `borg` key, see [how-to][].

[how-to]: HOWTO-debian-install.md

## Tips and tricks

### Single user mode, modern version

To enter something vaguely resembling "single user mode" when booting,
stop GRUB from booting Linux by pressing 'e' in the boot menu. Then
edit the line specifying the kernel and its arguments and add console
and systemd.unit as per below.


    linux /vmlinuz-VERSION ... console=ttyS0,115200 systemd.unit=rescue.target

If you don't want systemd at all, replace the systemd.unit argument with `init=/bin/sh`.


To enter rescue mode from a running system:

    systemctl isolate rescue.target

### Growing disks

Storage usage on /var and /usr tend to grow over time.

Detailed example of how we created a separate /var on tee.sigsum.org below.
Note that the LVM VG tee-vg already had free space.

```
  tee# sudo shutdown -h now
  logsrv-01# while virsh list | rg tee; do sleep 3; done
  logsrv-01# virsh start --console tee.sigsum.org
  e      # for editing kernel command line, adding console=ttyS0,115200 systemd.unit=rescue.target
  <tee root pw>
  tee# systemctl stop sytemd-journald
  tee# lsof | rg /var
  tee# lvcreate -n tee-var --size 10G tee-vg
  tee# mkfs.ext4 /dev/tee-vg/tee-var
  tee# blkid /dev/tee-vg/tee-var
  tee# echo 'UUID=69270b8f-9c56-4d10-bf59-7221380c9135 /var          ext4    defaults        0       2' >> /etc/fstab
root@tee:~# mv /var /var.orig; mkdir /var; mount -a; df -h /var
  tee# time cp -a /var.orig/* /var/   # took 10s
  tee# reboot
  tee# du -sh /var /var.orig; find /var | wc -l; find /var.orig | wc -l
  tee# rm -rf /var.orig; df -h /  # use=49%
```


### Listing virtual network interfaces on KVM host

    sudo virsh list |  rg '^ [0-9]+' | (while read -r id name _; do echo -en "$name\t"; virsh domiflist $id | grep vnet; done)
