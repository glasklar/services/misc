We don't have a better inventory than the list of nodes in the Ansible repo.
This will have to do for now.

# Debian cattle
- bup-01.glasklarteknik.se
- getuid.sigsum.org
- listen.sigsum.org
- madvise.sigsum.org
- pad.glasklar.is
- pause.sigsum.org
- poc.sigsum.org
  - deb https://ftp.acc.umu.se/debian bullseye-backports main
- sign-01.glasklarteknik.se
- tee.sigsum.org
  - deb https://ftp.acc.umu.se/debian bullseye-backports main

# Debian pets
- gitlab-01.sigsum.org
  - deb [signed-by=/usr/share/keyrings/gitlab_gitlab-ee-archive-keyring.gpg] https://packages.gitlab.com/gitlab/gitlab-ee/debian/ bullseye main
- mail-01.sigsum.org
  - deb [arch=amd64 signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian bullseye stable
- meet.sigsum.org
  - deb [signed-by=/usr/share/keyrings/jitsi-keyring.gpg] https://download.jitsi.org stable/
  - deb https://packages.prosody.im/debian bullseye main
- nextcloud-01.sigsum.org
  - deb https://apt.postgresql.org/pub/repos/apt bullseye-pgdg main

# Fedora
- build-03.glasklarteknik.se

# KVM hosts
- kvm-02.glasklarteknik.se
- logsrv-01.sigsum.org
- logsrv-02.sigsum.org

# Outliers
- stime.sigsum.org
  - ST testing, state by definition unknown
- mgmt-mlm-vg4-01.glasklarteknik.se
  - not ansiblified
- snapshot-mlm-01.debian.org
  - managed by DSA
