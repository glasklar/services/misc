Physical machines have storage, like HDDs and SSDs.

We ususally equip servers with storage units in pairs and create MD
RAID1 storage arrays (aka "mirrors") from these.

We use LVM to chop up the arrays into logical volumes (LV's) -- arrays
are added as LVM physical volumes (PV's) from which VG's are created=
from which LV's are created.

An exception from this is how we handle "system disks", ie the
typically smaller and faster storage units (NVMe when possible) used
for booting, OS and swap area. See HOWTO-debian-install.md for
details.

## Examples

Here's what we did with the SSD's on kvm-02.gtse.

    mdadm --create /dev/md/ssd0 -l 1 -n 2 /dev/sd[ab]
    pvcreate /dev/md/ssd0
    vgcreate vg_vm-images /dev/md/ssd0
    lvcreate -n qcows -L 1T vg_vm-images
    mkfs.ext4 /dev/vg_vm-images/qcows
    printf "%s\t/var/lib/libvirt/images\text4\tdefaults\t0\t2\n" $(blkid -o export /dev/vg_vm-images/qcows | grep ^UUID) >> /etc/fstab
    mount -a

### Snapshots

One way of restoring an LV to a certain state is to
1) create a snapshot of the LV in the certain state
2) let changes happen to the LV
3) **merge** the snapshot with the LV

Note that the merging will not happen until the LV is being
closed. Deactivating the LV using lvchange -a n VG/LV can be used to
make sure it's closed. The pending state of a merge can be hard to
determine.

In the following example, umount closes the LV.

```
root@logsrv-01:~# lvcreate -L 1g -n snaptest logsrv-01-vg
  Logical volume "snaptest" created.
root@logsrv-01:~# mkfs.ext4 /dev/logsrv-01-vg/snaptest
mke2fs 1.47.0 (5-Feb-2023)
Discarding device blocks: done
Creating filesystem with 262144 4k blocks and 65536 inodes
Filesystem UUID: 0d2eb88b-57f4-43dd-a2c3-5d975c013b1b
Superblock backups stored on blocks:
        32768, 98304, 163840, 229376

Allocating group tables: done
Writing inode tables: done
Creating journal (8192 blocks): done
Writing superblocks and filesystem accounting information: done

root@logsrv-01:~# mount /dev/logsrv-01-vg/snaptest /mnt
root@logsrv-01:~# echo before > /mnt/x
root@logsrv-01:~# lvcreate -L 500m --snapshot -n snaptest-snap logsrv-01-vg/snaptest
  Logical volume "snaptest-snap" created.
root@logsrv-01:~# echo after > /mnt/x
root@logsrv-01:~# cat /mnt/x
after
root@logsrv-01:~# umount /mnt
root@logsrv-01:~# lvconvert --mergesnapshot logsrv-01-vg/snaptest-snap
  Merging of volume logsrv-01-vg/snaptest-snap started.
  logsrv-01-vg/snaptest: Merged: 100.00%
root@logsrv-01:~# mount /dev/logsrv-01-vg/snaptest /mnt
root@logsrv-01:~# cat /mnt/x
before
root@logsrv-01:~# umount /mnt
root@logsrv-01:~# lvchange -a n logsrv-01-vg/snaptest
root@logsrv-01:~# lvremove logsrv-01-vg/snaptest
  Logical volume "snaptest" successfully removed.
```

### Moving /var to its own LV

Here's how bastion-01.gtse got a new disk device and moved /var to a
new LV on that device.

On the KVM host, creating the new LV:
```
root@logsrv-01:~# lvcreate -n bastion-01.glasklarteknik.se-vol1 --size 10g logsrv-01-ssd
  Logical volume "bastion-01.glasklarteknik.se-vol1" created.
```

Using virt-manager to attach the new LV to the VM:
- bastion-01.gtse
- Add Hardware
  - Select or create custom storage
    - Manage -> +
      - Name: logsrv-01-ssd
      - Type: logical: LVM Volume Group
      - Volgroup Name: logsrv-01-ssd
    - Finish
  - logsrv-01-ssd
  - bastion-01.glasklarteknik.se
  - Finish
    - "Could not be attached, would you like to make it available after next guest shutdown?"
	- Yes
- Shutdown
- Start

The rest happens in the VM

Making sure nobody's using /var:
```
root@bastion-01:~# systemctl isolate rescue.target
root@bastion-01:~# lsof | grep /var
systemd-j  261                           root  mem       REG              253,0  8388608     263132 /var/log/journal/4302b02a67404adb95a3adf02e6fe462/user-1000.journal
systemd-j  261                           root  mem       REG              253,0 67108864     260247 /var/log/journal/4302b02a67404adb95a3adf02e6fe462/system.journal
systemd-j  261                           root   23u      REG              253,0 67108864     260247 /var/log/journal/4302b02a67404adb95a3adf02e6fe462/system.journal
systemd-j  261                           root   32u      REG              253,0  8388608     263132 /var/log/journal/4302b02a67404adb95a3adf02e6fe462/user-1000.journal
root@bastion-01:~# systemctl stop systemd-journald
Warning: Stopping systemd-journald.service, but it can still be activated by:
systemd-journald.socket
systemd-journald-audit.socket
systemd-journald-dev-log.socket
hroot@bastion-01:~# !lsof
lsof | grep /var
root@bastion-01:~#
```

Creating PV, VG and LV:
```
lsblk
vgcreate vg-bastion-01-vol1 /dev/vdb
lvcreate -n var --size 5g vg-bastion-01-vol1
```

Creating a new filesystem for /var, copying /var to it, adding it to fstab and rebooting:
```
mkfs.ext4 /dev/vg-bastion-01-vol1/var
mount /dev/vg-bastion-01-vol1/var /mnt
rsync -a /var/ /mnt/
echo -e $(lsblk -Po UUID /dev/vg-bastion-01-vol1/var)\\t/var\\text4\\tdefaults\\t0\\t2 >> /etc/fstab
history -a; reboot
```

Cleaning up old /var:
```
root@bastion-01:~# df -h / /var
Filesystem                             Size  Used Avail Use% Mounted on
/dev/mapper/bastion--01--vg-root       6.4G  5.4G  704M  89% /
/dev/mapper/vg--bastion--01--vol1-var  4.9G  2.4G  2.3G  52% /var
root@bastion-01:~# mount --bind / /mnt
root@bastion-01:~# rm -rf /mnt/var/*
root@bastion-01:~# umount /mnt
root@bastion-01:~# df -h / /var
Filesystem                             Size  Used Avail Use% Mounted on
/dev/mapper/bastion--01--vg-root       6.4G  2.9G  3.2G  48% /
/dev/mapper/vg--bastion--01--vol1-var  4.9G  2.4G  2.3G  52% /var
```
