## listen.so

Packages and configuration files:

- Configuring libc6:amd64
  - `systemctl stop postfix`, then Y
- Configuration file '/etc/nftables.conf'
  - N
- Configuring unattended-upgrades
  - Keep
- Configuring grub-pc
  - Keep
- Configuring openssh-server
  - Keep
- Configuration file '/etc/apache2/apache2.conf'
  - N

After reboot, both `systemd --failed` and `journalctl -b -p warning` are happy.
I also checked that www.so, www.sto, www.gtse, and docs.sto all work again.

