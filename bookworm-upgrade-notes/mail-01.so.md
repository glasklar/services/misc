# mail-01.so 2024-07-12

## pre upgrade

Note that the Docker APT source will need manual handling.
```
root@mail-01:~# cat /etc/apt/sources.list.d/docker.list
deb [arch=amd64 signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian bullseye stable
```

Docker is at 27.0.2 while docker-compose is at 28.1.
```
root@mail-01:~# dpkg -l | rg docker
ii  docker-buildx-plugin           0.15.1-1~debian.11~bullseye      amd64        Docker Buildx cli plugin.
ii  docker-ce                      5:27.0.2-1~debian.11~bullseye    amd64        Docker: the open-source application container engine
ii  docker-ce-cli                  5:27.0.2-1~debian.11~bullseye    amd64        Docker CLI: the open-source application container engine
ii  docker-ce-rootless-extras      5:27.0.2-1~debian.11~bullseye    amd64        Rootless support for Docker.
ii  docker-compose-plugin          2.28.1-1~debian.11~bullseye      amd64        Docker Compose (V2) plugin for the Docker CLI.
ii  docker-scan-plugin             0.23.0~debian-bullseye           amd64        Docker scan cli plugin.
```

Seems like we won't need do update that PGP key:
```
root@mail-01:~# gpg --keyring /etc/apt/keyrings/docker.gpg --no-default-keyring --list-keys
/etc/apt/keyrings/docker.gpg
----------------------------
pub   rsa4096 2017-02-22 [SCEA]
      9DC858229FC7DD38854AE2D88D81803C0EBFCD88
uid           [ unknown] Docker Release (CE deb) <docker@docker.com>
sub   rsa4096 2017-02-22 [S]

root@mail-01:~# export D=$(mktemp -d)
root@mail-01:~# curl -s https://download.docker.com/linux/debian/gpg | GNUPGHOME=$D gpg --import
gpg: /tmp/tmp.QuBSg6cX5Y/trustdb.gpg: trustdb created
gpg: key 8D81803C0EBFCD88: public key "Docker Release (CE deb) <docker@docker.com>" imported
gpg: Total number processed: 1
gpg:               imported: 1
root@mail-01:~# GNUPGHOME=$D gpg --list-keys
/tmp/tmp.QuBSg6cX5Y/pubring.kbx
-------------------------------
pub   rsa4096 2017-02-22 [SCEA]
      9DC858229FC7DD38854AE2D88D81803C0EBFCD88
uid           [ unknown] Docker Release (CE deb) <docker@docker.com>
sub   rsa4096 2017-02-22 [S]

root@mail-01:~# rm -rf $D
```

Here's how we updated the APT sources:
```
root@mail-01:~# for f in sources.list sources.list.d/bullseye-backports.list sources.list.d/docker.list; do
        [ -f /etc/apt/"$f" ] || continue
    sed -i.bak -e 's/bullseye/bookworm/g' -e 's/ contrib//g' -e 's/ non-free//g' /etc/apt/"$f"
done
root@mail-01:~# cat /etc/apt/sources.list.d/docker.list
deb [arch=amd64 signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian bookworm stable
```

### storage
The disk partitioning is a little weird -- vda has a DOS partition table for some reason. Shouldn't matter much but looks strange.

```
root@mail-01:~# lsblk -f
NAME                    FSTYPE      FSVER    LABEL UUID                                   FSAVAIL FSUSE% MOUNTPOINT
vda
├─vda1                  ext2        1.0            9dc8d097-dc0b-4838-9d3a-38ed6d90eabb    358.7M    18% /boot
├─vda2
└─vda5                  LVM2_member LVM2 001       LICTMl-5zx4-60zQ-4TZG-Z2Hy-KoYd-FnumId
  ├─mail--01--vg-root   ext4        1.0            cdb8cf74-d145-4075-85f3-b1404cdef8b0      7.5G    53% /
  └─mail--01--vg-swap_1 swap        1              6b5c7c15-b7b4-4363-b59b-887f68d557cb                  [SWAP]
vdb                     LVM2_member LVM2 001       ieixHe-RGnH-aF3p-meLK-quMn-VPL3-AZ2USx
└─backup-backup         ext4        1.0            49e2875f-0a4c-49b8-9942-12b0a4e6a6d6      8.2G    24% /var/backups/mailcow
vdc                     LVM2_member LVM2 001       2o7cTV-w4Xc-KG0u-o3Ve-g93a-dY2q-ai9Cdl
└─vmail-vmail1          ext4        1.0            315405c3-a0fa-44ae-b071-437762cad598     10.6G     4% /var/lib/docker/volumes/mailcowdockerized_vmail-vol-1/_data
root@mail-01:~# parted /dev/vda p free
Model: Virtio Block Device (virtblk)
Disk /dev/vda: 21.5GB
Sector size (logical/physical): 512B/512B
Partition Table: msdos
Disk Flags:

Number  Start   End     Size    Type      File system  Flags
        1024B   1049kB  1048kB            Free Space
 1      1049kB  512MB   511MB   primary   ext2         boot
        512MB   513MB   1048kB            Free Space
 2      513MB   21.5GB  21.0GB  extended
 5      513MB   21.5GB  21.0GB  logical                lvm
        21.5GB  21.5GB  1049kB            Free Space
```
