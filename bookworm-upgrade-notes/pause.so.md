# Bookworm upgrade notes 

## Before the upgrade

No failed services right now (`systemctl --failed`).

No journalctl warnings this boot (`journalctl -b -p warning`).

Looking at `systemctl --type=service` some relevant things to keep in mind while
upgrading are: apache2, etherpad-lite, mariadb, tor (onion service).  The other
services looks pretty vanilla / things we have on most hosts.

Looking in ansible repo:
- certbots
- newapache
- onionservers
- pads
  - Corresponds to ansible role etherpad

I don't expect certbots, newapache, and onionservers to cause issues; we've
updated other systems that run these roles.

Short summary of some essential things the etherpad ansible role does:
- installs nodejs and node using apt
- extract etherpad from a pinned github release
- install database (mariadb) with apt
- configure mysql schema from a template
  - (Very simple schema, nothing that stands out)
- install etherpad dependencies with script from what was extracted
  - (I don't see any obvious installs here that stand out)
- configure more options that tune the etherpad service from template files
  - (I don't expect issues here because we will keep the etherpad version)
- wrap as a systemd service

Main worry when upgrading: will newer node versions work with our pinned
etherpad version.

```
root@pause:~# node --version
v12.22.12
root@pause:~# nodejs --version
v12.22.12
```

As a precaution before applying the bookworm upgrade Linus made an LVM snapshot.
We tested that Linus is able to roll back.  While doing this rollback testing
and now during the upgrade, it is only possible to reach pause.so with WG.

    ip  saddr @limit4 tcp dport {80, 443, 4722} accept
    ip6 saddr @limit6 tcp dport {80, 443, 4722} accept

## Upgrade notes

Using these instructions and notes as a starting point:
- https://git.glasklar.is/glasklar/services/misc/-/blob/main/HOWTO-debian-upgrade-bookworm.md
- https://git.glasklar.is/glasklar/services/misc/-/blob/main/bookworm-upgrade-notes/madvise.so.md

- Configuring unattended-upgrades
  - keep

NEWS when doing the full-upgrade:
- mariadb has a bunch of changes, none of which I expect to cause issues for us

- Configuring libc6:amd64
  - `systemctl stop postfix` (copying madvise notes)
  - yes
- Configuration file '/etc/default/atop'
  - keep
- Configuration file '/etc/nftables.conf'
  - keep
- Configuring openssh-server
  - keep
- Configuration file '/etc/apache2/apache2.conf'
  - keep
- Configuring grub-pc
  - keep

## After the upgrade

```
root@pause:~# systemctl --failed
  UNIT                  LOAD   ACTIVE SUB    DESCRIPTION
* etherpad-lite.service loaded failed failed Etherpad-lite, the collaborative editor.

LOAD   = Reflects whether the unit definition was properly loaded.
ACTIVE = The high-level unit activation state, i.e. generalization of SUB.
SUB    = The low-level unit activation state, values depend `
```

At a closer look in the journal, this looks like the issue:

```
[2024-08-06 19:51:25.570] [ERROR] console - Fatal MySQL error: Error: connect ECONNREFUSED ::1:3306
```

mariadb is up and running and seems to be working as expected, but note:
```
# ss -tupln
Netid     State      Recv-Q     Send-Q         Local Address:Port           Peer Address:Port     Process
udp       UNCONN     0          0                  127.0.0.1:8125                0.0.0.0:*         users:(("netdata",pid=1152,fd=80))
udp       UNCONN     0          0                  127.0.0.1:53                  0.0.0.0:*         users:(("unbound",pid=485,fd=5))
udp       UNCONN     0          0                      [::1]:8125                   [::]:*         users:(("netdata",pid=1152,fd=79))
udp       UNCONN     0          0                      [::1]:53                     [::]:*         users:(("unbound",pid=485,fd=3))
tcp       LISTEN     0          4096               127.0.0.1:19999               0.0.0.0:*         users:(("netdata",pid=1152,fd=6))
tcp       LISTEN     0          100                  0.0.0.0:25                  0.0.0.0:*         users:(("master",pid=1148,fd=13))
tcp       LISTEN     0          4096               127.0.0.1:8125                0.0.0.0:*         users:(("netdata",pid=1152,fd=96))
tcp       LISTEN     0          80                 127.0.0.1:3306                0.0.0.0:*         users:(("mariadbd",pid=557,fd=15))
tcp       LISTEN     0          256                127.0.0.1:53                  0.0.0.0:*         users:(("unbound",pid=485,fd=6))
tcp       LISTEN     0          128                  0.0.0.0:4722                0.0.0.0:*         users:(("sshd",pid=467,fd=3))
tcp       LISTEN     0          511                        *:443                       *:*         users:(("apache2",pid=606,fd=6),("apache2",pid=605,fd=6),("apache2",pid=604,fd=6))
tcp       LISTEN     0          4096                   [::1]:8125                   [::]:*         users:(("netdata",pid=1152,fd=95))
tcp       LISTEN     0          100                     [::]:25                     [::]:*         users:(("master",pid=1148,fd=14))
tcp       LISTEN     0          511                        *:80                        *:*         users:(("apache2",pid=606,fd=4),("apache2",pid=605,fd=4),("apache2",pid=604,fd=4))
tcp       LISTEN     0          256                    [::1]:53                     [::]:*         users:(("unbound",pid=485,fd=4))
tcp       LISTEN     0          128                     [::]:4722                   [::]:*         users:(("sshd",pid=467,fd=4))
```

I.e., mariadb does not listen on [::1]:3306; so makes sense that the connect fails.

Looking in our ansible we don't seem to have much mariadb config.  We do
configure etherpad's credentials.json file though, which currently says to
connect to mariadb with "localhost". Changing this to "127.0.0.1" -> works.

I'll make the corresponding change in ansible to persist the change.

## Sanity checks

```
root@pause:~# reboot
root@pause:~# systemctl --failed
  UNIT LOAD ACTIVE SUB DESCRIPTION
0 loaded units listed.
root@pause:~# journalctl -b -p warning
Aug 06 20:23:16 pause kernel: acpi PNP0A03:00: fail to add MMCONFIG information, can't access extended PCI configuration space under this bridge.
Aug 06 20:23:16 pause kernel: sym0: No NVRAM, ID 7, Fast-40, LVD, parity checking
Aug 06 20:23:16 pause kernel: device-mapper: core: CONFIG_IMA_DISABLE_HTABLE is disabled. Duplicate IMA measurements will not be recorded in the IMA log.
Aug 06 20:23:16 pause lvm[277]: PV /dev/vda5 online, VG pause-vg is complete.
Aug 06 20:23:16 pause lvm[277]: VG pause-vg finished
Aug 06 20:23:17 pause kernel: ext2 filesystem being mounted at /boot supports timestamps until 2038 (0x7fffffff)
Aug 06 20:23:31 pause Tor[837]: HiddenServiceNonAnonymousMode is set. Every hidden service on this tor instance is NON-ANONYMOUS. If the HiddenServiceNonAnonymousMode option is changed,>
Aug 06 20:23:31 pause Tor[837]: This copy of Tor was compiled or configured to run in a non-anonymous mode. It will provide NO ANONYMITY.
```

A bit unsure about some of the above warnings, but at least no etherpad
failures.  Maybe the above is related to Linus' snapshot configuration?

Continued end-to-end checking:

- [x] Can read an old pad I expect to exist
- [x] Can write in a new pad
- [x] Can create new pad from template

Now removing the requirement on having a WG tunnel, then checking also:

- [x] Can access pad.so with Tor Browser
- [x] Can access pad.so's onion service with Tor Browser

## Final remarks

Before this upgrade it was possible to SSH pause.so without a wireguard tunnel.
We now use the default nftables configuration that requires a WG tunnel; but
allow access to web ports 80 and 443 without WG.  Web ports is configured in:

    /etc/nftables.conf.d/web.conf

Also note that this was merely a bullseye -> bookworm upgrade.  **The pinned
etherpad version has not been bumped, but it probably should be bumped too.**
