# Bookworm upgrade notes

## Before the upgrade

```
# systemctl --failed
  UNIT LOAD ACTIVE SUB DESCRIPTION
0 loaded units listed.
# systemctl --type=service
[snip]
```

Didn't see anything unexpected / extra when looking at what we're deploying
with ansible. Other than what all our hosts have, getuid basically contains
cgit, gitolite, apache2, onion service, and certbot.

```
# journalctl -b -p warning
[snip]
```

There are a bunch of warnings, none which looks particularly bad though.

## Upgrade notes

Using these notes for reference when upgrading:
https://git.glasklar.is/glasklar/services/misc/-/blob/main/HOWTO-debian-upgrade-bookworm.md?ref_type=heads#instructions

- Configuring libc6:amd64
  - `systemctl stop postfix` (copying madvise notes)
  - yes
- Configuring base-passwd
  - no (see prompt and reasoning behind it below)
- Configuration file '/etc/nftables.conf'
  - keep
- Configuring unattended-upgrades
  - keep (we have two more lines than the default configured)
- Configuring grub-pc
  - keep

Both my SSH connections to getuid.so died during `apt full-upgrade`, and unable
to reach the host now.  The last thing I saw was:

```
Unpacking systemd (252.26-1~deb12u2) over (247.3-7+deb11u5) ...
```

Doesn't seem like a WG problem:

```
$ traceroute getuid.sigsum.org
traceroute to getuid.sigsum.org (91.223.231.168), 30 hops max, 60 byte packets
 1  10.47.11.1 (10.47.11.1)  10.334 ms  10.292 ms  10.277 ms
 2  glasklar-gw.verkligendata.se (91.223.231.161)  2959.575 ms !H  2959.598 ms !H  2959.582 ms !H
```

Will resort to console from logsrv-01.so.

Console from logsrv-01.so also hangs (unable to login).

`virsh shutdown` didn't respond (waited ~45m before trying this).  `virsh
destroy --graceful` worked, now doing `virsh start` to see if I can get a
console.
- Works

Not finding much of a clue of what went wrong when looking at:
- /var/log/apt/term.log
  - Final line: `Unpacking libperl5.36:amd64 (5.36.0-7+deb12u1) ...`
- /var/log/apt/history.log
  - Last command: `Commandline: apt full-upgrade` (2024-08-04  20:19:25)

From journalctl after we started the upgrade:
```
Aug 04 20:19:29 getuid dbus-daemon[2591214]: Unable to reload configuration: Failed to open "/usr/share/dbus-1/system.conf": No such file or directory
```

Then the next line is new boot.  The missing file exists now without me doing
anything (other than the forceful reboot).

This looks related from `/var/log/syslog`:
```
2024-08-04T20:19:29.122230+02:00 getuid dbus-daemon[2591214]: [system] Reloaded configuration
2024-08-04T20:19:29.148376+02:00 getuid dbus-daemon[2591214]: [system] Reloaded configuration
2024-08-04T20:19:29.148437+02:00 getuid dbus-daemon[2591214]: [system] Reloaded configuration
2024-08-04T20:19:29.148470+02:00 getuid dbus-daemon[2591214]: [system] Reloaded configuration
2024-08-04T20:19:29.148509+02:00 getuid dbus-daemon[2591214]: [system] Reloaded configuration
2024-08-04T20:19:29.163110+02:00 getuid dbus-daemon[2591214]: [system] Reloaded configuration
2024-08-04T20:19:29.288639+02:00 getuid dbus-daemon[2591214]: [system] Unable to reload configuration: Failed to open "/usr/share/dbus-1/system.conf": No such file or directory
2024-08-04T20:19:29.290050+02:00 getuid dbus-daemon[2591214]: Unable to reload configuration: Failed to open "/usr/share/dbus-1/system.conf": No such file or directory
2024-08-04T20:19:30.051451+02:00 getuid dbus-daemon[2591214]: [system] Reloaded configuration
2024-08-04T20:19:30.064226+02:00 getuid dbus-daemon[2591214]: [system] Reloaded configuration
2024-08-04T20:19:30.136126+02:00 getuid dbus-daemon[2591214]: [system] Reloaded configuration
2024-08-04T20:19:30.138297+02:00 getuid dbus-daemon[2591214]: [system] Reloaded configuration
^@^@ [snip]
```

Then the next entry is from the next boot.

There's nothing relevant in `/var/log/messages`.

Not sure how fatal it is for dbus-daemon to not work, but I'm guessing it will
mess with communication on the host.  Unclear why the error happened.  Not sure
what other logs would be useful to look at to debug the cause further.

`apt list --upgradable` still shows ~100 packages to be upgraded.

```
# dpkg --configure -a
Errors were encountered while processing:
 libnss-systemd:amd64
 systemd-timesyncd
 libpam-systemd:amd64
# apt --fix-broken install
# dpkg --configure -a
```

Now, let's try upgrade with `apt full-upgrade`.

- Configuring openssh-server
  - keep
- Configuration file '/etc/apache2/apache2.conf'
  - keep

Now the rest of the upgrade instructions worked as expected until reboot.

### base-passwd prompt in more detail

```
update-passwd has found a difference between your system accounts and the
current Debian defaults.  It is advisable to allow update-passwd to change
your system; without those changes some packages might not work correctly.
For more documentation on the Debian account policies, please see
/usr/share/doc/base-passwd/README.

The proposed change is:
Change the GID of user "www-data" from 1001 (git) to 33 (www-data)

[snip]. If you do not make this change now, you can make it later with the
update-passwd utility.
```

Saying "no" here, because it seems reasonable that we intentionally configured
the www-data user to have access to git stuff on this host.

## Sanity checks

- [x] Have bookworm release
  - `cat /etc/debian_version`
- [x] Can ssh to getuid.so
- [x] systemctl --failed
  - Nothing to see here
- [x] journalctl -b -p warning
  - Nothing to see here
- [x] Access git.so with Tor Browser, and click OL button to visit onion service
- [x] Try running ansible from a host
  - Successfully ran ansible on bastion-01.gtse
