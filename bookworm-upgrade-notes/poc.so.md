## poc.so

### Before

```
root@poc:~# systemctl --failed
  UNIT                                   LOAD   ACTIVE SUB    DESCRIPTION
  ● sigsum-log-secondary@jellyfish.service loaded failed failed Sigsum log
  secondary node

  LOAD   = Reflects whether the unit definition was properly loaded.
  ACTIVE = The high-level unit activation state, i.e. generalization of SUB.
  SUB    = The low-level unit activation state, values depend on unit type.
  1 loaded units listed.
root@poc:~# journalctl -b -p warning
[snip]
Jul 01 19:17:23 poc.sigsum.org systemd[1]: sigsum-log-secondary@jellyfish.service: Failed with result 'exit-code'.
Jul 01 19:17:24 poc.sigsum.org systemd[1]: sigsum-log-secondary@jellyfish.service: Failed with result 'exit-code'.
Jul 01 19:17:24 poc.sigsum.org systemd[1]: sigsum-log-secondary@jellyfish.service: Failed with result 'exit-code'.
Jul 01 19:17:24 poc.sigsum.org systemd[1]: sigsum-log-secondary@jellyfish.service: Failed with result 'exit-code'.
Jul 01 19:17:24 poc.sigsum.org systemd[1]: sigsum-log-secondary@jellyfish.service: Failed with result 'exit-code'.
Jul 01 19:17:25 poc.sigsum.org systemd[1]: sigsum-log-secondary@jellyfish.service: Start request repeated too quickly.
Jul 01 19:17:25 poc.sigsum.org systemd[1]: sigsum-log-secondary@jellyfish.service: Failed with result 'exit-code'.
Jul 01 19:17:25 poc.sigsum.org systemd[1]: Failed to start Sigsum log secondary node.
```

Only a primary runs on poc.so.  And this primary does not run with a secondary,
see `/var/sigsum/sigsum/.config/sigsum/jellyfish/config.toml`.  And to the best
of my knowledge we're not running any other secondary on poc.so.  So, we might
see these errors after the upgrade; and if so I will ignore them.

### Notes

After peeking with `crontab -l`, this looks how to run `my_bup.sh` here:
```
BUP_POST_HOOK_FILE="/usr/bin/mariarmsnap.sh" BUP_PRE_HOOK_FILE="/usr/bin/mariasnap.sh" BUP_RATE_LIMIT=- my_bup.sh
```

(It did not work without the pre/post hooks, but it worked with them added.)

Packages and configuration files:
- Configuration file '/etc/letsencrypt/cli.ini'
  - N
  - See the \[LE\] notes below for what upstream tried to provide us
- Configuration file '/etc/nftables.conf'
  - N
- Configuring openssh-server
  - K
- Configuration file '/etc/mysql/mariadb.conf.d/50-server.cnf'
  - N
  - See the \[MariaDB\] notes below
- Configuring grub-pc
  - K

\[LE\]:
```
*** cli.ini (Y/I/N/O/D/Z) [default=N] ? D
--- /etc/letsencrypt/cli.ini    1970-01-01 01:00:00.000000000 +0100
+++ /etc/letsencrypt/cli.ini.dpkg-new   2021-11-12 04:51:27.000000000 +0100
@@ -0,0 +1,5 @@
+# Because we are using logrotate for greater flexibility, disable the
+# internal certbot logrotation.
+max-log-backups = 0
+# Adjust interactive output regarding automated renewal
+preconfigured-renewal = True
```

MariaDB release notes:
```
[snip]
Important packaging change: Compression libraries have been split into
separate packages named mariadb-provider-plugin-(bzip2/lz4/lzma/lzo/snappy).
If a non-zlib compression algorithm was used in InnoDB or Mroonga before
upgrading to 10.11, those tables will be unreadable until the appropriate
compression library is installed.

Things to consider when upgrading from 10.6 to 10.11 are listed on the page
https://mariadb.com/kb/en/upgrading-from-mariadb-10-6-to-mariadb-10-11/.
[snip]
```

\[MariaDB\]:
```
$ cat /etc/mysql/mariadb.conf.d/50-server.cnf.dpkg-new
#
# These groups are read by MariaDB server.
# Use it for options that only the server (but not clients) should see

# this is read by the standalone daemon and embedded servers
[server]

# this is only for the mysqld standalone daemon
[mysqld]

#
# * Basic Settings
#

#user                    = mysql
pid-file                = /run/mysqld/mysqld.pid
basedir                 = /usr
#datadir                 = /var/lib/mysql
#tmpdir                  = /tmp

# Broken reverse DNS slows down connections considerably and name resolve is
# safe to skip if there are no "host by domain name" access grants
#skip-name-resolve

# Instead of skip-networking the default is now to listen only on
# localhost which is more compatible and is not less secure.
bind-address            = 127.0.0.1

#
# * Fine Tuning
#

#key_buffer_size        = 128M
#max_allowed_packet     = 1G
#thread_stack           = 192K
#thread_cache_size      = 8
# This replaces the startup script and checks MyISAM tables if needed
# the first time they are touched
#myisam_recover_options = BACKUP
#max_connections        = 100
#table_cache            = 64

#
# * Logging and Replication
#

# Note: The configured log file or its directory need to be created
# and be writable by the mysql user, e.g.:
# $ sudo mkdir -m 2750 /var/log/mysql
# $ sudo chown mysql /var/log/mysql

# Both location gets rotated by the cronjob.
# Be aware that this log type is a performance killer.
# Recommend only changing this at runtime for short testing periods if needed!
#general_log_file       = /var/log/mysql/mysql.log
#general_log            = 1

# When running under systemd, error logging goes via stdout/stderr to journald
# and when running legacy init error logging goes to syslog due to
# /etc/mysql/conf.d/mariadb.conf.d/50-mysqld_safe.cnf
# Enable this if you want to have error logging into a separate file
#log_error = /var/log/mysql/error.log
# Enable the slow query log to see queries with especially long duration
#log_slow_query_file    = /var/log/mysql/mariadb-slow.log
#log_slow_query_time    = 10
#log_slow_verbosity     = query_plan,explain
#log-queries-not-using-indexes
#log_slow_min_examined_row_limit = 1000

# The following can be used as easy to replay backup logs or for replication.
# note: if you are setting up a replication slave, see README.Debian about
#       other settings you may need to change.
#server-id              = 1
#log_bin                = /var/log/mysql/mysql-bin.log
expire_logs_days        = 10
#max_binlog_size        = 100M

#
# * SSL/TLS
#

# For documentation, please read
# https://mariadb.com/kb/en/securing-connections-for-client-and-server/
#ssl-ca = /etc/mysql/cacert.pem
#ssl-cert = /etc/mysql/server-cert.pem
#ssl-key = /etc/mysql/server-key.pem
#require-secure-transport = on

#
# * Character sets
#

# MySQL/MariaDB default is Latin1, but in Debian we rather default to the full
# utf8 4-byte character set. See also client.cnf
character-set-server  = utf8mb4
collation-server      = utf8mb4_general_ci

#
# * InnoDB
#

# InnoDB is enabled by default with a 10MB datafile in /var/lib/mysql/.
# Read the manual for more InnoDB related options. There are many!
# Most important is to give InnoDB 80 % of the system RAM for buffer use:
# https://mariadb.com/kb/en/innodb-system-variables/#innodb_buffer_pool_size
#innodb_buffer_pool_size = 8G

# this is only for embedded server
[embedded]

# This group is only read by MariaDB servers, not by MySQL.
# If you use the same .cnf file for MySQL and MariaDB,
# you can put MariaDB-only options here
[mariadb]

# This group is only read by MariaDB-10.11 servers.
# If you use the same .cnf file for MariaDB of different versions,
# use this group for options that older servers don't understand
[mariadb-10.11]
```

Looks like the added warning (in comments) about having the direcotory looks OK
for us:

```
# ls -la /var/log/mysql
total 40
drwxr-s---  2 mysql adm  4096 Oct  6  2021 .
drwxr-xr-x 14 root  root 4096 Jul  1 20:17 ..
-rw-r-----  1 mysql adm   724 Oct  6  2021 error.log
-rw-r-----  1 mysql adm    20 Oct  5  2021 error.log.1.gz
-rw-r-----  1 mysql adm    20 Oct  4  2021 error.log.2.gz
-rw-r-----  1 mysql adm    20 Oct  3  2021 error.log.3.gz
-rw-r-----  1 mysql adm    20 Oct  2  2021 error.log.4.gz
-rw-r-----  1 mysql adm    20 Oct  1  2021 error.log.5.gz
-rw-r-----  1 mysql adm    20 Sep 30  2021 error.log.6.gz
-rw-r-----  1 mysql adm    20 Sep 29  2021 error.log.7.gz
```

The below looks like a change we may care about if we get errors:

```
-#slow_query_log_file    = /var/log/mysql/mariadb-slow.log
-#long_query_time        = 10
+#log_slow_query_file    = /var/log/mysql/mariadb-slow.log
+#log_slow_query_time    = 10
```

I don't see this in
https://mariadb.com/kb/en/upgrading-from-mariadb-10-6-to-mariadb-10-11/#options-that-have-been-removed-or-renamed
though, so we're probably fine not updating our config.

The below also looks important:
```
-# This group is only read by MariaDB-10.5 servers.
+# This group is only read by MariaDB-10.11 servers.
 # If you use the same .cnf file for MariaDB of different versions,
 # use this group for options that older servers don't understand
-[mariadb-10.5]
+[mariadb-10.11]
```

But, this section is empty for us. So should be OK to leave as is.

---

No errors from `systemctl --failed` and `journalctl -b -p warning` after reboot.

Looks like jellyfish at least started again, and it's being correctly cosigned
by rgdd's witness.

Nisse's witness did not start.  Debugging:

```
root@poc:/home/poc-witness# sudo -u poc-witness /home/poc-witness/start-witness.sh
Identity added: config/witness-key (config/witness-key)
Traceback (most recent call last):
  File "/home/poc-witness/.local/bin/poetry", line 5, in <module>
    from poetry.console.application import main
ModuleNotFoundError: No module named 'poetry'
```

Perhaps poetry was pip-installed globally before.  Trying:
```
# apt install python3-poetry
```

Same issue with `flask`:
```
# apt install python3-flask
```

Still does not work though:

```
root@poc:/home/poc-witness# python3 -m flask --version
Python 3.11.2
Flask 2.2.2
Werkzeug 2.2.2
root@poc:/home/poc-witness# sudo -u poc-witness /home/poc-witness/start-witness.sh
Identity added: config/witness-key (config/witness-key)
Traceback (most recent call last):
  File "/home/poc-witness/sigsum-py/./sigsum_witness.py", line 30, in <module>
    import flask
ModuleNotFoundError: No module named 'flask'
```

Ah, looking in nisse's script the right thing is probably to install
dependencies with poetry (`poetry install`).  Adding one line for that.

Diff:
```
root@poc:/home/poc-witness# diff /home/poc-witness/start-witness.sh /home/poc-witness/start-witness.sh.bak
14c14
< cd sigsum-py && poetry install && poetry run ./sigsum_witness.py ${WITNESS_ARGS} -d ${HOME}/config --ssh-agent \
---
> cd sigsum-py && poetry run ./sigsum_witness.py ${WITNESS_ARGS} -d ${HOME}/config --ssh-agent \
```

Reverting `apt install python3-flask`. Keeping `python3-poetry`.

Now sudo -u poc-witness /home/poc-witness/start-witness.sh starts.  And it
works to stop and start again.  And it starts as expected on reboot (tested).

Also verified that the getting started guide on www.so works.  This included
submission and downloading the entire log with the sigsum monitor.

Conclusion: nothing seems to be obviously wrong with poc.so.

