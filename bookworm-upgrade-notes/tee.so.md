## tee.so

- note: ansible will override the changes to apt sources (just "main").  Leaving
  as is for now though, it is equally easy to revert again to just "main" later.
- note: `apt -o APT::Get::Trivial-Only=true full-upgrade` does not run, but the
  two following commands is what's in the Debian guide. Using just them instead.

Packages
- Configuration file '/etc/network/if-down.d/resolvconf'
  - N (we have deleted this file, upstream have a new version)
- Configuration file '/etc/network/if-up.d/000resolvconf'
  - N (we have deleted this file, upstream have a new version)
- 20auto-upgrades
  - K
- Configuring libc6:amd64
  - y
  - `systemctl stop postfix` before saying yes (copying madvise notes)
- Samba server and utilities
  - N
  - (Diff -> looks like our own customizations we want to keep)
- Configuration file '/etc/network/if-down.d/postfix'
  - N (we have deleted this file, upstream have a new version)
- Configuration file '/etc/network/if-up.d/postfix'
  - N (we have deleted this file, upstream have a new version)
- Configuration file '/etc/nftables.conf'
  - N
- Configuring openssh-server
  - K
  - See also https://git.glasklar.is/glasklar/services/misc/-/issues/25
- Configuration file '/etc/apache2/apache2.conf'
  - N
  - (Managed by ansible)
- Configuring grub-pc
  - K

After reboot:

```
root@tee:~# systemctl --failed
  UNIT             LOAD   ACTIVE SUB    DESCRIPTION
● openipmi.service loaded failed failed LSB: OpenIPMI Driver init script

LOAD   = Reflects whether the unit definition was properly loaded.
ACTIVE = The high-level unit activation state, i.e. generalization of SUB.
SUB    = The low-level unit activation state, values depend on unit type.
1 loaded units listed.
```

This doesn't look right:
```
root@tee:~# dmesg | grep ipmi
[    3.454512] ipmi device interface
[    3.477686] ipmi_si: IPMI System Interface driver
[    3.477728] ipmi_si: Unable to find any System Interface(s)
[snip]
root@tee:~# lsmod | grep ipmi
ipmi_devintf           20480  0
ipmi_msghandler        77824  1 ipmi_devintf
root@tee:~# modprobe ipmi_si
modprobe: ERROR: could not insert 'ipmi_si': No such device
```

Solution: uninstall ipmi stuff, we're not using this on tee.
- `apt purge ipmitool openipmi freeipmi-common libfreeipmi17`
- `apt autoremove`
- `apt purge \~c`

There's also an udev issue:
```
# journalctl -b -p warning
[snip]
Jul 01 00:04:12 tee systemd-udevd[300]: /etc/udev/rules.d/50-yubihsm2.rules:9 Unknown group 'YOURUSER', ignoring
```

I added the entire file's content in comments, because it is clearly
misconfigured and probably not used by anyone.
