## logsrv-01.so 2024-07-11
apt full-upgrade resulted in https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1074789
NOTE: Skipped restarting of running processes after `apt full-upgrade` which might've been unwise.

Missing socket:

    root@logsrv-01:~# lsof /run/systemd/userdb/io.systemd.DynamicUser
	root@logsrv-01:~#

systemd should be listening:

	root@logsrv-02:~# lsof /run/systemd/userdb/io.systemd.DynamicUser
	COMMAND PID USER   FD   TYPE             DEVICE SIZE/OFF NODE NAME
	systemd   1 root   20u  unix 0x00000000073f00d1      0t0  239 /run/systemd/userdb/io.systemd.DynamicUser type=STREAM (LISTEN)

daemon-reexec doesn't help:

	root@logsrv-01:~# systemctl daemon-reexec
	root@logsrv-01:~# lsof /run/systemd/userdb/io.systemd.DynamicUser

There's a new systemd on its way
https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1074789#107 but it's
not available in stable (as of 2024-07-11).

systemd does complain about "No space lef on device" when reloading,
but we're not particularly short on space on any mounted filesystem
and I don't know enough about systemd scope units to understand the
error message.

```
Jul 11 22:49:50 logsrv-01 systemd[1]: Reloading.
Jul 11 22:49:50 logsrv-01 systemd[1]: machine-qemu\x2d3\x2dgitlab\x2d01.sigsum.org.scope: Failed to set 'cpuset.mems' attribute on '/machine.slice/machine-qemu\x2d3\x2dgitlab\x2d01.sigsum.org.scope' to '': No space left on device
Jul 11 22:49:50 logsrv-01 systemd[1]: machine-qemu\x2d7\x2dlisten.sigsum.org.scope: Failed to set 'cpuset.mems' attribute on '/machine.slice/machine-qemu\x2d7\x2dlisten.sigsum.org.scope' to '': No space left on device
Jul 11 22:49:50 logsrv-01 systemd[1]: machine-qemu\x2d1\x2dbuild\x2d03.glasklarteknik.se.scope: Failed to set 'cpuset.mems' attribute on '/machine.slice/machine-qemu\x2d1\x2dbuild\x2d03.glasklarteknik.se.scope' to '': No space left on device
Jul 11 22:49:50 logsrv-01 systemd[1]: machine-qemu\x2d4\x2dpad.glasklar.is.scope: Failed to set 'cpuset.mems' attribute on '/machine.slice/machine-qemu\x2d4\x2dpad.glasklar.is.scope' to '': No space left on device
Jul 11 22:49:50 logsrv-01 systemd[1]: machine-qemu\x2d10\x2dpoc.sigsum.org.scope: Failed to set 'cpuset.mems' attribute on '/machine.slice/machine-qemu\x2d10\x2dpoc.sigsum.org.scope' to '': No space left on device
Jul 11 22:49:50 logsrv-01 systemd[1]: machine-qemu\x2d11\x2dmeet.sigsum.org.scope: Failed to set 'cpuset.mems' attribute on '/machine.slice/machine-qemu\x2d11\x2dmeet.sigsum.org.scope' to '': No space left on device
Jul 11 22:49:50 logsrv-01 systemd[1]: machine-qemu\x2d6\x2dbastion\x2d01.glasklarteknik.se.scope: Failed to set 'cpuset.mems' attribute on '/machine.slice/machine-qemu\x2d6\x2dbastion\x2d01.glasklarteknik.se.scope' to '': No space left on device
Jul 11 22:49:50 logsrv-01 systemd[1]: machine-qemu\x2d12\x2dsign\x2d01.glasklarteknik.se.scope: Failed to set 'cpuset.mems' attribute on '/machine.slice/machine-qemu\x2d12\x2dsign\x2d01.glasklarteknik.se.scope' to '': No space left on device
Jul 11 22:49:50 logsrv-01 systemd[1]: machine-qemu\x2d9\x2dmail\x2d01.sigsum.org.scope: Failed to set 'cpuset.mems' attribute on '/machine.slice/machine-qemu\x2d9\x2dmail\x2d01.sigsum.org.scope' to '': No space left on device
Jul 11 22:49:50 logsrv-01 systemd[1]: machine-qemu\x2d2\x2dnextcloud\x2d01.sigsum.org.scope: Failed to set 'cpuset.mems' attribute on '/machine.slice/machine-qemu\x2d2\x2dnextcloud\x2d01.sigsum.org.scope' to '': No space left on device
Jul 11 22:49:50 logsrv-01 systemd[1]: machine-qemu\x2d13\x2dpause.sigsum.org.scope: Failed to set 'cpuset.mems' attribute on '/machine.slice/machine-qemu\x2d13\x2dpause.sigsum.org.scope' to '': No space left on device
Jul 11 22:49:50 logsrv-01 systemd[1]: machine-qemu\x2d5\x2dtee.sigsum.org.scope: Failed to set 'cpuset.mems' attribute on '/machine.slice/machine-qemu\x2d5\x2dtee.sigsum.org.scope' to '': No space left on device
Jul 11 22:49:50 logsrv-01 systemd[1]: machine-qemu\x2d8\x2dgetuid.sigsum.org.scope: Failed to set 'cpuset.mems' attribute on '/machine.slice/machine-qemu\x2d8\x2dgetuid.sigsum.org.scope' to '': No space left on device
```

Ended up disabling dynamic getenv by editing /etc/nsswitch.conf,
cf. nss-systemd(8), https://systemd.io/USER_GROUP_API,
https://varlink.org/, and reconfiguring:
```
root@logsrv-01:~# cp /etc/nsswitch.conf  /etc/nsswitch.conf.BAK
root@logsrv-01:~# vi /etc/nsswitch.conf
root@logsrv-01:~# diff -u /etc/nsswitch.conf.BAK /etc/nsswitch.conf
--- /etc/nsswitch.conf.BAK      2024-07-11 23:13:30.874561696 +0200
+++ /etc/nsswitch.conf  2024-07-11 23:13:40.770391548 +0200
@@ -4,8 +4,8 @@
 # If you have the `glibc-doc-reference' and `info' packages installed, try:
 # `info libc "Name Service Switch"' for information about this file.

-passwd:         files systemd
-group:          files systemd
+passwd:         files
+group:          files
 shadow:         files
 gshadow:        files
root@logsrv-01:~# dpkg --configure -a
Setting up polkitd (122-3) ...
Creating group 'polkitd' with GID 996.
Creating user 'polkitd' (polkit) with UID 996 and GID 996.
Setting up polkitd-pkla (122-3) ...
Setting up pkexec (122-3) ...
Setting up modemmanager (1.20.4-1) ...
Created symlink /etc/systemd/system/dbus-org.freedesktop.ModemManager1.service → /lib/systemd/system/ModemManager.service.
Created symlink /etc/systemd/system/multi-user.target.wants/ModemManager.service → /lib/systemd/system/ModemManager.service.
Setting up libvirt-daemon-system (9.0.0-4) ...
Installing new version of config file /etc/apparmor.d/abstractions/libvirt-qemu ...
Installing new version of config file /etc/apparmor.d/usr.lib.libvirt.virt-aa-helper ...
Installing new version of config file /etc/apparmor.d/usr.sbin.libvirtd ...
Installing new version of config file /etc/default/libvirtd ...
Installing new version of config file /etc/default/virtlockd ...
Installing new version of config file /etc/default/virtlogd ...
Installing new version of config file /etc/libvirt/libvirtd.conf ...
Installing new version of config file /etc/libvirt/qemu.conf ...
Installing new version of config file /etc/sasl2/libvirt.conf ...
virtlockd.service is a disabled or a static unit, not starting it.
virtlogd.service is a disabled or a static unit, not starting it.
Setting up policykit-1 (122-3) ...
Removing obsolete conffile /etc/pam.d/polkit-1 ...
Removing obsolete conffile /etc/polkit-1/localauthority.conf.d/50-localauthority.conf ...
Removing obsolete conffile /etc/polkit-1/localauthority.conf.d/51-debian-sudo.conf ...
Processing triggers for dbus (1.14.10-1~deb12u1) ...
root@logsrv-01:~# mv /etc/nsswitch.conf.BAK /etc/nsswitch.conf
```

### root@logsrv-01:~# apt full-upgrade
```
Setting up polkitd (122-3) ...
Failed to check if group polkitd already exists: Connection refused
id: ‘polkitd’: no such user
chown: invalid user: ‘polkitd:root’
dpkg: error processing package polkitd (--configure):
 installed polkitd package post-installation script subprocess returned error exit status 1
dpkg: dependency problems prevent configuration of polkitd-pkla:
 polkitd-pkla depends on polkitd (>= 121+compat0.1); however:
  Package polkitd is not configured yet.

dpkg: error processing package polkitd-pkla (--configure):
 dependency problems - leaving unconfigured
dpkg: dependency problems prevent configuration of pkexec:
 pkexec depends on polkitd (= 122-3); however:
  Package polkitd is not configured yet.

dpkg: error processing package pkexec (--configure):
 dependency problems - leaving unconfigured
dpkg: dependency problems prevent configuration of libvirt-daemon-system:
 libvirt-daemon-system depends on polkitd (>= 121+compat0.1-2); however:
  Package polkitd is not configured yet.

dpkg: error processing package libvirt-daemon-system (--configure):
 dependency problems - leaving unconfigured
dpkg: dependency problems prevent configuration of policykit-1:
 policykit-1 depends on pkexec (= 122-3); however:
  Package pkexec is not configured yet.
 policykit-1 depends on polkitd (= 122-3); however:
  Package polkitd is not configured yet.

dpkg: error processing package policykit-1 (--configure):
 dependency problems - leaving unconfigured
dpkg: dependency problems prevent configuration of modemmanager:
 modemmanager depends on polkitd | policykit-1; however:
  Package polkitd is not configured yet.
  Package policykit-1 is not configured yet.

dpkg: error processing package modemmanager (--configure):
 dependency problems - leaving unconfigured
Processing triggers for dictionaries-common (1.29.5) ...
Processing triggers for libgdk-pixbuf-2.0-0:amd64 (2.42.10+dfsg-1+deb12u1) ...
Processing triggers for mariadb-server (1:10.11.6-0+deb12u1) ...
Processing triggers for shim-signed:amd64 (1.39+15.7-1) ...
Errors were encountered while processing:
 polkitd
 polkitd-pkla
 pkexec
 libvirt-daemon-system
 policykit-1
 modemmanager
```
