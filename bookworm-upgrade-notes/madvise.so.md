## madvise.so

### Configuring packages
- /etc/apt/apt.conf.d/20auto-upgrades: keep (currently installed version)
- [[ missing one or two that i failed to note down, but action was keep local file]]
- mailman3: no
  - database for mailman3
  - dbconfig-common, /usr/share/doc/mailman3
- libc6: yes
  - systemctl stop postfix mailman3 mailman3-web postgresql before selecting yes
- libc6:amd64: none
  - removed all suggested restarts (ssh and cron)
- base-passwd: no
  - update-passwd wants to change home for user 'list' from /var/lib/mailman3 to /var/list, see
    /usr/share/doc/base-passwd/README
  - TODO: consider making the change using update-passwd after the upgrade is done
    UPDATE: At least one mailman config file points at /var/lib/mailman3, Apache does that too.
	UPDATE: Moving this to a separate issue.
- /etc/rsyslog.conf: install (the package mainteainer's version)
- postgresql-common: information about postgresql clusters
  - /usr/share/doc/postgresql-common/README.Debian.gz
  - we're not using clustering and can ignore this
- /etc/nftables.conf: keep
- /etc/sshd_config: keep
  - noting that PidFile has a new default (/var/run/sshd.pid -> /run/sshd.pid)
- mailman3 (again): no
- /etc/mailman3/mailman.cfg: keep
- /etc/default/grub: keep

### New versions of relevant packages

```
root@madvise:/etc/mailman3# dpkg -l | rg 'mailman|python3-django|hyperkitty|postgres'
ii  mailman3                        3.3.8-2~deb12u1                all          Mailing list management system
ii  mailman3-web                    0+20200530-2.1                 all          Django project integrating Mailman3 Postorius and HyperKitty
ii  postgresql                      15+248                         all          object-relational SQL database (supported version)
ii  postgresql-13                   13.13-0+deb11u1                amd64        The World's Most Advanced Open Source Relational Database
ii  postgresql-15                   15.5-0+deb12u1                 amd64        The World's Most Advanced Open Source Relational Database
ii  postgresql-client-13            13.13-0+deb11u1                amd64        front-end programs for PostgreSQL 13
ii  postgresql-client-15            15.5-0+deb12u1                 amd64        front-end programs for PostgreSQL 15
ii  postgresql-client-common        248                            all          manager for multiple PostgreSQL client versions
ii  postgresql-common               248                            all          PostgreSQL database-cluster manager
ii  postgresql-doc                  15+248                         all          documentation for the PostgreSQL database management system
ii  postgresql-doc-13               13.13-0+deb11u1                all          documentation for the PostgreSQL database management system
ii  postgresql-doc-15               15.5-0+deb12u1                 all          documentation for the PostgreSQL database management system
ii  python3-django                  3:3.2.19-1+deb12u1             all          High-level Python web development framework
ii  python3-django-allauth          0.51.0-1                       all          Django app for local and social authentication (Python 3 version)
ii  python3-django-appconf          1.0.5-2                        all          helper class handling configuration defaults of apps - Python 3.x
ii  python3-django-compressor       4.0-1                          all          Compresses linked, inline JS or CSS into single cached files - Python 3.x
ii  python3-django-extensions       3.2.1-2                        all          Useful extensions for Django projects (Python 3 version)
ii  python3-django-filters          23.1-1                         all          filter Django QuerySets based on user selections (Python3)
ii  python3-django-gravatar2        1.4.4-4                        all          Python3 library that provides essential Gravatar support
ii  python3-django-guardian         2.4.0-2                        all          per object permissions of django for Python3
ii  python3-django-haystack         3.2.1-1                        all          modular search for Django (Python version)
ii  python3-django-hyperkitty       1.3.7-1                        all          Web user interface to access GNU Mailman3 archives
ii  python3-django-mailman3         1.3.9-1                        all          Django library to help interaction with Mailman3 (Python 3 version)
ii  python3-django-picklefield      3.1.0-1                        all          Pickled object field for Django (Python3 version)
ii  python3-django-postorius        1.3.8-3                        all          Web user interface to access GNU Mailman3
ii  python3-django-q                1.3.9-4                        all          Django multiprocessing distributed task queue (Python 3 version)
ii  python3-djangorestframework     3.14.0-2                       all          Web APIs for Django, made easy for Python3
ii  python3-mailman-hyperkitty      1.2.1-1                        all          Mailman3 plugin to archive emails with HyperKitty
ii  python3-mailmanclient           3.3.5-1                        all          Python bindings for Mailman3 REST API (Python 3 version)
```

Selected updates, from apt logs:
```
mailman3:amd64 (3.3.3-1, 3.3.8-2~deb12u1)
mailman3-web:amd64 (0+20200530-2, 0+20200530-2.1)
python3-django-hyperkitty:amd64 (1.3.4-4, 1.3.7-1)
python3-django-mailman3:amd64 (1.3.5-2, 1.3.9-1)
python3-mailmanclient:amd64 (3.3.2-1, 3.3.5-1)
```

### mailman3.service not starting
```
Dec 07 09:51:09 madvise mailman3[480]: sqlalchemy.exc.NoSuchModuleError: Can't load plugin: sqlalchemy.dialects:postgres

root@madvise:/etc/mailman3# rg 'postgres.*://'
mailman.cfg
152:url: postgres://mailman3:vjNY39MOLhnz2XUpaa@localhost/mailman3
root@madvise:/etc/mailman3# sed -i.bak -e 's!postgres://!postgresql://!1' mailman.cfg
root@madvise:/etc/mailman3# systemctl start mailman3
```

Needs fixing in Ansible


### Archives not working
https://lists.system-transparency.org/mailman3/hyperkitty/list/st-announce@lists.system-transparency.org/ gives "Server error"

```
django.db.utils.ProgrammingError: column hyperkitty_mailinglist.archive_rendering_mode does not exist
LINE 1: ...e_policy", "hyperkitty_mailinglist"."created_at", "hyperkitt...
```

Did what I should've done earlier: mailman-web migrate (see below).

Archives now work.

### mailman-web migrate

https://docs.mailman3.org/en/latest/upgrade-guide.html says one should
do a bunch of mailman-web things. So we did, see below.

The django_q timeout > retry warning is https://github.com/Koed00/django-q/issues/526 and can be ignored.

#### root@madvise:/etc/mailman3# mailman-web migrate
```
/usr/lib/python3/dist-packages/django_q/conf.py:139: UserWarning: Retry and timeout are misconfigured. Set retry larger than timeout,
        failure to do so will cause the tasks to be retriggered before completion.
        See https://django-q.readthedocs.io/en/latest/configure.html#retry for details.
  warn(

System check identified some issues:
WARNINGS:
django_mailman3.MailDomain: (models.W042) Auto-created primary key used when not defining a primary key type, by default 'django.db.models.AutoField'.
        HINT: Configure the DEFAULT_AUTO_FIELD setting or the DjangoMailman3Config.default_auto_field attribute to point to a subclass of AutoField, e.g. 'django.db.models.BigAutoField'.
django_mailman3.Profile: (models.W042) Auto-created primary key used when not defining a primary key type, by default 'django.db.models.AutoField'.
        HINT: Configure the DEFAULT_AUTO_FIELD setting or the DjangoMailman3Config.default_auto_field attribute to point to a subclass of AutoField, e.g. 'django.db.models.BigAutoField'.
hyperkitty.Attachment: (models.W042) Auto-created primary key used when not defining a primary key type, by default 'django.db.models.AutoField'.
        HINT: Configure the DEFAULT_AUTO_FIELD setting or the HyperKittyConfig.default_auto_field attribute to point to a subclass of AutoField, e.g. 'django.db.models.BigAutoField'.
hyperkitty.Email: (models.W042) Auto-created primary key used when not defining a primary key type, by default 'django.db.models.AutoField'.
        HINT: Configure the DEFAULT_AUTO_FIELD setting or the HyperKittyConfig.default_auto_field attribute to point to a subclass of AutoField, e.g. 'django.db.models.BigAutoField'.
hyperkitty.Favorite: (models.W042) Auto-created primary key used when not defining a primary key type, by default 'django.db.models.AutoField'.
        HINT: Configure the DEFAULT_AUTO_FIELD setting or the HyperKittyConfig.default_auto_field attribute to point to a subclass of AutoField, e.g. 'django.db.models.BigAutoField'.
hyperkitty.LastView: (models.W042) Auto-created primary key used when not defining a primary key type, by default 'django.db.models.AutoField'.
        HINT: Configure the DEFAULT_AUTO_FIELD setting or the HyperKittyConfig.default_auto_field attribute to point to a subclass of AutoField, e.g. 'django.db.models.BigAutoField'.
hyperkitty.MailingList: (models.W042) Auto-created primary key used when not defining a primary key type, by default 'django.db.models.AutoField'.
        HINT: Configure the DEFAULT_AUTO_FIELD setting or the HyperKittyConfig.default_auto_field attribute to point to a subclass of AutoField, e.g. 'django.db.models.BigAutoField'.
hyperkitty.Profile: (models.W042) Auto-created primary key used when not defining a primary key type, by default 'django.db.models.AutoField'.
        HINT: Configure the DEFAULT_AUTO_FIELD setting or the HyperKittyConfig.default_auto_field attribute to point to a subclass of AutoField, e.g. 'django.db.models.BigAutoField'.
hyperkitty.Tag: (models.W042) Auto-created primary key used when not defining a primary key type, by default 'django.db.models.AutoField'.
        HINT: Configure the DEFAULT_AUTO_FIELD setting or the HyperKittyConfig.default_auto_field attribute to point to a subclass of AutoField, e.g. 'django.db.models.BigAutoField'.
hyperkitty.Tagging: (models.W042) Auto-created primary key used when not defining a primary key type, by default 'django.db.models.AutoField'.
        HINT: Configure the DEFAULT_AUTO_FIELD setting or the HyperKittyConfig.default_auto_field attribute to point to a subclass of AutoField, e.g. 'django.db.models.BigAutoField'.
hyperkitty.Thread: (models.W042) Auto-created primary key used when not defining a primary key type, by default 'django.db.models.AutoField'.
        HINT: Configure the DEFAULT_AUTO_FIELD setting or the HyperKittyConfig.default_auto_field attribute to point to a subclass of AutoField, e.g. 'django.db.models.BigAutoField'.
hyperkitty.ThreadCategory: (models.W042) Auto-created primary key used when not defining a primary key type, by default 'django.db.models.AutoField'.
        HINT: Configure the DEFAULT_AUTO_FIELD setting or the HyperKittyConfig.default_auto_field attribute to point to a subclass of AutoField, e.g. 'django.db.models.BigAutoField'.
hyperkitty.Vote: (models.W042) Auto-created primary key used when not defining a primary key type, by default 'django.db.models.AutoField'.
        HINT: Configure the DEFAULT_AUTO_FIELD setting or the HyperKittyConfig.default_auto_field attribute to point to a subclass of AutoField, e.g. 'django.db.models.BigAutoField'.
postorius.EmailTemplate: (models.W042) Auto-created primary key used when not defining a primary key type, by default 'django.db.models.AutoField'.
        HINT: Configure the DEFAULT_AUTO_FIELD setting or the PostoriusConfig.default_auto_field attribute to point to a subclass of AutoField, e.g. 'django.db.models.BigAutoField'.
Operations to perform:
  Apply all migrations: account, admin, auth, contenttypes, django_mailman3, django_q, hyperkitty, postorius, sessions, sites, socialaccount
Running migrations:
  Applying auth.0012_alter_user_first_name_max_length... OK
  Applying django_q.0010_auto_20200610_0856... OK
  Applying django_q.0011_auto_20200628_1055... OK
  Applying django_q.0012_auto_20200702_1608... OK
  Applying django_q.0013_task_attempt_count... OK
  Applying django_q.0014_schedule_cluster... OK
  Applying hyperkitty.0022_mailinglist_archive_rendering_mode... OK
  Applying hyperkitty.0023_alter_mailinglist_name... OK
```

#### root@madvise:/etc/mailman3# mailman-web compress
```
/usr/lib/python3/dist-packages/django_q/conf.py:139: UserWarning: Retry and timeout are misconfigured. Set retry larger than timeout,
        failure to do so will cause the tasks to be retriggered before completion.
        See https://django-q.readthedocs.io/en/latest/configure.html#retry for details.
  warn(
Compressing... done
Compressed 2 block(s) from 104 template(s) for 1 context(s).
```

#### root@madvise:/etc/mailman3# mailman-web collectstatic
```
/usr/lib/python3/dist-packages/django_q/conf.py:139: UserWarning: Retry and timeout are misconfigured. Set retry larger than timeout,
        failure to do so will cause the tasks to be retriggered before completion.
        See https://django-q.readthedocs.io/en/latest/configure.html#retry for details.
  warn(

You have requested to collect static files at the destination
location as specified in your settings:

    /var/lib/mailman3/web/static

This will overwrite existing files!
Are you sure you want to do this?

Type 'yes' to continue, or 'no' to cancel: yes

88 static files copied to '/var/lib/mailman3/web/static', 315 unmodified.
```

#### root@madvise:/etc/mailman3# mailman-web compilemessages
```
/usr/lib/python3/dist-packages/django_q/conf.py:139: UserWarning: Retry and timeout are misconfigured. Set retry larger than timeout,
        failure to do so will cause the tasks to be retriggered before completion.
        See https://django-q.readthedocs.io/en/latest/configure.html#retry for details.
  warn(
CommandError: Can't find msgfmt. Make sure you have GNU gettext tools 0.15 or newer installed.
```

### Postgresql 13 -> 15

We haven't done anything about postgresql 13 -> 15 yet.

This showed to not be critical for getting back to status quo.

```
root@madvise:/etc/mailman3# pg_lsclusters
Ver Cluster Port Status Owner    Data directory              Log file
13  main    5432 online postgres /var/lib/postgresql/13/main /var/log/postgresql/postgresql-13-main.log
15  main    5433 online postgres /var/lib/postgresql/15/main /var/log/postgresql/postgresql-15-main.log
```

Looking at services postgresql@13-main and postgresql@15-main I don't
think (WARNING: this showed to be false) we're using clustering and
can safely uninstall 13:

```
root@madvise:/etc/mailman3# apt remove postgresql-13 postgresql-client-13 postgresql-doc-13
root@madvise:/etc/mailman3# apt autopurge
```

Rebooting as well, for good measure.

This revealed two things, one small and one bigger.

1. The fix for sqlalchemy deprecating 'postgres', chaning to
   'postgresql' was not in Ansible. Fixed it.

2. mailman fails when trying to open db on port 5432

For fixing 2 it's tempting to simply change some configuration
somewhere but maybe we should look into db migration first.

The problem with following
f.ex. https://unix.stackexchange.com/questions/735327/upgrade-your-postgresql-database-from-14-to-15-on-debian
is that we don't have -13 running anymore and we can not start it
because we uninstalled it and purged its deps.

In hindsight, systemctl disable --now postgresql-13 and systemctl
restart mailman3 would have been smarter.

Now, do we need 13 running in order to run pg_upgradecluster?

```
root@madvise:/etc/mailman3# pg_lsclusters
Ver Cluster Port Status                Owner    Data directory              Log file
13  main    5432 down,binaries_missing postgres /var/lib/postgresql/13/main /var/log/postgresql/postgresql-13-main.log
15  main    5433 online                postgres /var/lib/postgresql/15/main /var/log/postgresql/postgresql-15-main.log

Dropped 15 and tried upgrading 13:
root@madvise:/etc/mailman3# pg_dropcluster --stop 15 main
root@madvise:/etc/mailman3# pg_upgradecluster 13 main
Error: pg_controldata not found, please install postgresql-13
```

Here's what's been removed:
```
Remove: postgresql-13:amd64 (13.13-0+deb11u1), postgresql-client-13:amd64 (13.13-0+deb11u1), postgresql-doc-13:amd64 (13.13-0+deb11u1)
Purge: libllvm11:amd64 (1:11.0.1-2), libldap-2.4-2:amd64 (2.4.57+dfsg-3+deb11u1), libicu67:amd64 (67.1-7)
```

Added bullseye main to apt sources:

    echo "deb https://ftp.acc.umu.se/debian/ bullseye main" > /etc/apt/sources.list.d/bullseye-temp.list

What would be installed when asking for postgresql-13:amd64 looks reasonable:

    libicu67 libldap-2.4-2 libllvm11 postgresql-13 postgresql-client-13

Installing -13 again.

```
root@madvise:/etc/mailman3# pg_lsclusters
Ver Cluster Port Status Owner    Data directory              Log file
13  main    5432 online postgres /var/lib/postgresql/13/main /var/log/postgresql/postgresql-13-main.log
```

Running pg_upgradecluster 13 (see below for details) main then fixed
everything, including moving the tcp ports around so that 15 now has
the port 13 used to have.

It did not drop the 13 cluster though. Before doing that, we removed
the 13 binaries (and the bullseye apt source) again and restart
mailman3. Then

    pg_dropcluster 13 main

and the directory /var/lib/postgresql/13 disappeared and pg_lsclusters list only 15.

Remains to verify that the db backup script still does a reasonable thing.

```
root@madvise:/etc/mailman3# cat /usr/local/sbin/pgsql-dump.sh
#! /bin/sh
set -eu

table="$1"; shift
/usr/bin/pg_dump -Fc "$table" > "/var/backups/postgres/${table}.pg_dump"
```

That looks reasonable and the resulting file shrinked just a little
after the script was run. This is in user 'postgres' crontab btw so
here's how to test it reasonably close to reality:

    sudo -u postgres /usr/local/sbin/pgsql-dump.sh mailman3

On a side note, my_bup.sh is exiting with 1 even when things are
good. Probably because of the test for a post hook file
failing. Fixed the cleanup() function, see commit 

#### root@madvise:/etc/mailman3# pg_upgradecluster 13 main
```
Stopping old cluster...
Restarting old cluster with restricted connections...
Notice: extra pg_ctl/postgres options given, bypassing systemctl for start operation
Creating new PostgreSQL cluster 15/main ...
/usr/lib/postgresql/15/bin/initdb -D /var/lib/postgresql/15/main --auth-local peer --auth-host scram-sha-256 --no-instructions --encoding SQL_ASCII --lc-collate C --lc-ctype C
The files belonging to this database system will be owned by user "postgres".
This user must also own the server process.

The database cluster will be initialized with locale "C".
The default text search configuration will be set to "english".

Data page checksums are disabled.

fixing permissions on existing directory /var/lib/postgresql/15/main ... ok
creating subdirectories ... ok
selecting dynamic shared memory implementation ... posix
selecting default max_connections ... 100
selecting default shared_buffers ... 128MB
selecting default time zone ... Europe/Stockholm
creating configuration files ... ok
running bootstrap script ... ok
performing post-bootstrap initialization ... ok
syncing data to disk ... ok

Copying old configuration files...
Copying old start.conf...
Copying old pg_ctl.conf...
Starting new cluster...
Notice: extra pg_ctl/postgres options given, bypassing systemctl for start operation
Roles, databases, schemas, ACLs...
 set_config
------------

(1 row)

 set_config
------------

(1 row)

 set_config
------------

(1 row)

 set_config
------------

(1 row)

Fixing hardcoded library paths for stored procedures...
Upgrading database template1...
Analyzing database template1...
Fixing hardcoded library paths for stored procedures...
Upgrading database postgres...
Analyzing database postgres...
Fixing hardcoded library paths for stored procedures...
Upgrading database mailman3...
Analyzing database mailman3...
Stopping target cluster...
Stopping old cluster...
Disabling automatic startup of old cluster...
Starting upgraded cluster on port 5432...

Success. Please check that the upgraded cluster works. If it does,
you can remove the old cluster with
    pg_dropcluster 13 main

Ver Cluster Port Status Owner    Data directory              Log file
13  main    5433 down   postgres /var/lib/postgresql/13/main /var/log/postgresql/postgresql-13-main.log
Ver Cluster Port Status Owner    Data directory              Log file
15  main    5432 online postgres /var/lib/postgresql/15/main /var/log/postgresql/postgresql-15-main.log
```

